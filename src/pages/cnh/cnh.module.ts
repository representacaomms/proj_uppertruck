import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CnhPage } from './cnh';

@NgModule({
  declarations: [
    CnhPage,
  ],
  imports: [
    IonicPageModule.forChild(CnhPage),
  ],
})
export class CnhPageModule {}
