import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { DocVeiculoPage } from '../doc-veiculo/doc-veiculo';

@IonicPage()
@Component({
  selector: 'page-cnh',
  templateUrl: 'cnh.html',
})
export class CnhPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private firebaseProvider: FirebaseProvider,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController) {   

  }

  async takePhoto() {
   
		try {
			const options: CameraOptions = {
				quality: 50,
				targetHeight: 600,
				targetWidth: 600,
				destinationType: this.camera.DestinationType.DATA_URL,
				encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        cameraDirection: 0,
				correctOrientation: true
			};

			const result = await this.camera.getPicture(options);

      const image = `data:image/jpeg;base64,${result}`;
      
      this.firebaseProvider.uploadImage(this.navParams.get('dados'),image, 'cnh').then( data => {
      this.proximaPagina(this.navParams.get('dados'));
      }).catch(e => console.error(e))


		} catch (e) {
			console.error(e);
		} 
  }
  proximaPagina(value){
   const loader = this.loadingCtrl.create({
      content: "Por Favor, Aguarde...",
      duration: 3000
    });
    this.navCtrl.push(DocVeiculoPage, { dados: value})
    loader.present();
  }

}

