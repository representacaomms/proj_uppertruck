import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FotoVeiculoPage } from './foto-veiculo';

@NgModule({
  declarations: [
    FotoVeiculoPage,
  ],
  imports: [
    IonicPageModule.forChild(FotoVeiculoPage),
  ],
})
export class FotoVeiculoPageModule {}
