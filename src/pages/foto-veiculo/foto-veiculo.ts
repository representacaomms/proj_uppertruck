import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-foto-veiculo',
  templateUrl: 'foto-veiculo.html',
})
export class FotoVeiculoPage {
  device: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private firebaseProvider: FirebaseProvider,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController, private storage: Storage) {   
      storage.get('device').then((val) => {
        this.device = val;
      });
  }

  async takePhoto() {
   
		try {
			const options: CameraOptions = {
				quality: 50,
				targetHeight: 600,
				targetWidth: 600,
				destinationType: this.camera.DestinationType.DATA_URL,
				encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        cameraDirection: 0,
				correctOrientation: true
			};

			const result = await this.camera.getPicture(options);

      const image = `data:image/jpeg;base64,${result}`;
      
      this.firebaseProvider.uploadImage(this.navParams.get('dados'),image, 'foto-veiculo').then( data => {
      this.proximaPagina(this.navParams.get('dados'));
      }).catch(e => console.error(e))


		} catch (e) {
			console.error(e);
		} 
  }
  proximaPagina(value){
    this.firebaseProvider.create('cad_Aplicativo', {'cnpj':value, 'device': this.device})

   const loader = this.loadingCtrl.create({
      content: "Por Favor, Aguarde...",
      duration: 3000
    });

    let alert = this.alertCtrl.create({
      title: 'Seu cadastro será analisado por nossa equipe!',
      subTitle: 'Por favor, aguarde a liberação',
      buttons: [ {
        text: 'OK',
        handler: () => {
          this.navCtrl.setRoot(LoginPage);
        }
        
        }]
      });
      alert.present();
    
    loader.present();
  }

}
