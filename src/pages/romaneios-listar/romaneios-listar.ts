import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ColetasPage } from '../coletas/coletas';



@IonicPage()
@Component({
  selector: 'page-romaneios-listar',
  templateUrl: 'romaneios-listar.html',
})
export class RomaneiosListarPage  implements OnInit{
  getRomaneios: any = [];
  listar: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ngOnInit() {

    this.storage.get('romaneios').then( (val) =>{
      for (const key in val) {
        const element = val[key];
        if(element.coleta.status == "Fechado"){
          this.getRomaneios.push(element);
        }
      }
    
       
    }) 
    
    setTimeout(() => {
      console.log('Romaneios: ', this.getRomaneios)

    }, 500);
  }

  selecionar(value){
    this.navCtrl.push(ColetasPage, { dados: value})
  }

  

}
