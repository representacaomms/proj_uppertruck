import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RomaneiosListarPage } from './romaneios-listar';

@NgModule({
  declarations: [
    RomaneiosListarPage,
  ],
  imports: [
    IonicPageModule.forChild(RomaneiosListarPage),
  ],
})
export class RomaneiosListarPageModule {}
