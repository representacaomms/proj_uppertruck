import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EspacoVeiculoPage } from './espaco-veiculo';

@NgModule({
  declarations: [
    EspacoVeiculoPage,
  ],
  imports: [
    IonicPageModule.forChild(EspacoVeiculoPage),
  ],
})
export class EspacoVeiculoPageModule {}
