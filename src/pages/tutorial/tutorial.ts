import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelfiePage } from '../selfie/selfie';
import { Home_02Page } from '../home-02/home-02';
import { InformacaoPage } from '../informacao/informacao';



@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  proximo(){

    this.navCtrl.setRoot(InformacaoPage, { device: this.navParams.get('device')})
  }

}
