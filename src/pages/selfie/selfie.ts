import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { CnhPage } from '../cnh/cnh';
import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions } from "@ionic-native/camera-preview";
import { SelfieFotoPage } from '../selfie-foto/selfie-foto';

@IonicPage()
@Component({
	selector: 'page-selfie',
	templateUrl: 'selfie.html',
})
export class SelfiePage  implements OnInit{
	cpf:any;
	picture:any;
	  // picture options
	pictureOpts: CameraPreviewPictureOptions = {
		width: 1280,
		height: 1280,
		quality: 85,
		
	}

	cameraPreviewOpts: CameraPreviewOptions = {
		x: 0,
		y: 0,
		width: window.screen.width,
		height: window.screen.height,
		camera: 'rear',
		tapPhoto: true,
		previewDrag: true,
		toBack: true,
		alpha: 1
	
	  };

	constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private firebaseProvider: FirebaseProvider,
		private alertCtrl: AlertController, private loadingCtrl: LoadingController, private cameraPreview : CameraPreview,) {
			
		}

	ngOnInit(){
		const cameraPreviewOpts: CameraPreviewOptions = {
			x: 0,
			y: 0,
			width: window.screen.width,
			height: window.screen.height,
			camera: 'rear',
			tapPhoto: true,
			previewDrag: true,
			toBack: true,
			alpha: 1
		  };
		  
		  // start camera
		  this.cameraPreview.startCamera(cameraPreviewOpts).then(
			(res) => {
			  console.log(res)
			  
			  this.cameraPreview.switchCamera();

			  this.iniciarCamera()

			  
			},
			(err) => {
			  console.log(err)
			});
		
		
	}	


	getUsuario(cpf) {

		this.navCtrl.push(SelfieFotoPage, {cpf: cpf})
	}
	iniciarCamera(){
			this.cameraPreview.show();
	}


}



