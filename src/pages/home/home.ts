import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, NavParams } from 'ionic-angular';
import { RomaneiosPage } from '../romaneios/romaneios';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Storage } from '@ionic/storage';


declare var google;

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage  {

	entregas: any = [];
	qtdentregas: any = [];
	coletas: any = {};
	servicos: any = {};
	parceiros: any = {};
	data: any = {};

	lat: any;
	lng: any;
	cpf: any;
	id_motorista: any;

	lottieConfig: any;

	client: any;

	firstname;
	lastname;
	record;
	position;
	dadosMotorista: any = {};
	id_localizacao: any;
	getAllLocalizacao: any = {};

	posicaoCAM: any = {};

	constructor(public navCtrl: NavController,  public toastCtrl: ToastController, public alertCtrl: AlertController,private geolocation: Geolocation,
		public navParams: NavParams, private firebaseProvider: FirebaseProvider, private backgroundGeolocation: BackgroundGeolocation, private storage: Storage ) {
			

	}	
	updateGeolocation(dados, lat, lng) {
		this.storage.get('mykey').then(val => {
			if(val === null){
			const value = this.firebaseProvider.createPosicao(dados, lat, lng);
			this.storage.set('mykey', value)
			} else {
				this.firebaseProvider.atualizarPosicao(val,dados, lat, lng);
			}
		})	 	

	}

	
	minhaLocalizacao() {
	 	this.geolocation.getCurrentPosition().then((resp) => {
			let dados ={ nome: this.dadosMotorista.nome, cpf: this.dadosMotorista.cpf}
			this.updateGeolocation( dados, resp.coords.latitude, resp.coords.longitude);
		}).catch((error) => {
			console.log('Error getting location', error);
		});
		setTimeout(() => {
			this.minhaLocalizacaoAutom();
		}, 1000); 

	}

	minhaLocalizacaoAutom() {
		let watch = this.geolocation.watchPosition();
		watch.subscribe((data) => {
			this.posicaoCAM.lat =  data.coords.latitude;
			this.posicaoCAM.lng = data.coords.longitude;
			let dados ={ nome: this.dadosMotorista.nome, cpf: this.dadosMotorista.cpf}
			this.updateGeolocation(dados, data.coords.latitude, data.coords.longitude);
		}); 
	}

 
	abrirRomaneios() {
		this.navCtrl.push(RomaneiosPage);
	}	

	abrirParceiros() {
		const alert = this.alertCtrl.create({
			title: 'Olá Amigo!',
			subTitle: 'Estamos Desenvolvendo!',
			buttons: ['OK']
		});
		alert.present();
	} 

	ionViewDidLoad() {
		this.firebaseProvider.getAllDoc('romaneios').then((snapshot) => {
			const items = snapshot.val();
			for (const key in items) {
				if(items[key]['coleta']['status'] === "aberto"){
					items[key].id = key;
					this.qtdentregas.push(items[key])
				} 			
			}			
		});
		this.minhaLocalizacao();
		this.storage.get('dados').then(data =>{
			this.dadosMotorista = data
		})
		this.startBackgroundGeolocation()
	}

	 startBackgroundGeolocation() {
		this.backgroundGeolocation.isLocationEnabled()
			.then((rta) => {
				if (rta) {
					this.start();
				} else {
					this.backgroundGeolocation.showLocationSettings();
				}
			})
	}

	 start() {

		const config: BackgroundGeolocationConfig = {
			desiredAccuracy: 10,
			stationaryRadius: 1,
			distanceFilter: 1,
			debug: true,
			stopOnTerminate: false,
			// Android only section
			locationProvider: 1,
			startForeground: true,
			interval: 6000,
			fastestInterval: 5000,
			activitiesInterval: 10000
		};

		console.log('start');

		this.backgroundGeolocation
			.configure(config)
			.subscribe((location: BackgroundGeolocationResponse) => {
				let watch = this.geolocation.watchPosition();
				watch.subscribe(() => {
					let dados = { 'nome': this.dadosMotorista.nome, 'cpf': this.dadosMotorista.cpf }
					this.updateGeolocation(dados,location.latitude, location.longitude);

				});
			});

		// start recording location
		this.backgroundGeolocation.start();

	}

	stopBackgroundGeolocation() {
		this.backgroundGeolocation.stop();
	}  



}
