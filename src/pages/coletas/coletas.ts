import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

//import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker } from '@ionic-native/google-maps';
import { Storage } from '@ionic/storage';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { EntregasPage } from '../entregas/entregas';
import { Http } from '@angular/http';



declare var google;


@IonicPage()
@Component({
    selector: 'page-coletas',
    templateUrl: 'coletas.html',
})
export class ColetasPage {

    item: any = {};
    dados: any = {};
    data: any = {};
    coletas: any = {}
    distancia: any;
    tempo: any;

    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer(); 
    map: any;
    startPosition: any;
    originPosition: any;
    destinationPosition: string;
    id_motorista: any;
    cpf: any;
    coords: any = {};
    lat: any;
    lng: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, /* public service: ServiceProvider, */ public storage: Storage, public http: Http, 
         private launchNavigator: LaunchNavigator,  public alertCtrl: AlertController , private geolocation: Geolocation ) {
        let dados = this.navParams.get('dados');
        this.dados = dados;
        this.cpf = dados.cpf;
        console.log(this.dados)

    }

    ionViewDidLoad() {            
            this.initializeMap();
    }

    initializeMap() {
        
        this.geolocation.getCurrentPosition().then((resp) => {
            this.startPosition = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            console.log('dentro do mapa' + resp.coords.latitude + resp.coords.longitude) 
            this.data.lat = resp.coords.latitude
            this.data.lng = resp.coords.longitude

            const mapOptions = {
                zoom: 18,
                center: this.startPosition,
                styles: [
                    { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
                    {
                        featureType: 'administrative.locality',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [{ color: '#263c3f' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#6b9a76' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [{ color: '#38414e' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#212a37' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#9ca5b3' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [{ color: '#746855' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#1f2835' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#f3d19c' }]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'geometry',
                        stylers: [{ color: '#2f3948' }]
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [{ color: '#17263c' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#515c6d' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.stroke',
                        stylers: [{ color: '#17263c' }]
                    }
                ]
            }
    
            this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            this.directionsDisplay.setMap(this.map);
    
            const marker = new google.maps.Marker({
                position: this.startPosition,
                map: this.map,
            });
    
            setTimeout(() => {
                     this.calculateRoute(resp.coords.latitude,resp.coords.longitude);
                    this.calcularDistancia(resp.coords.latitude,resp.coords.longitude)
            }, 1000);
		}).catch((error) => {
			console.log('Error getting location', error);
		}); 
       
    }

    calcularDistancia(lat,lng) {
            this.http.get(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${lat},${lng}&destinations=${this.dados.coleta.endereco_cliente}&key=AIzaSyAt9KtdFvn2WCY7xo_Jf9IkjNLIXh1gSvQ`)
                .subscribe(data => {
                    console.log(data)
                    let dados = JSON.parse(data['_body']);
                    this.distancia = dados['rows'][0]['elements'][0]['distance'].text;
                    this.tempo = dados['rows'][0]['elements'][0]['duration'].text;
                }) 



    }

    calculateRoute(lat, lng) {
        const request = {
            // Pode ser uma coordenada (LatLng), uma string ou um lugar
            origin: this.startPosition,
            destination: this.dados.coleta.endereco_cliente,
            travelMode: 'DRIVING'
        };

       this.traceRoute(this.directionsService, this.directionsDisplay, request);

    }

    traceRoute(service: any, display: any, request: any) {
        service.route(request, function (result, status) {
            if (status == 'OK') {
                display.setDirections(result);
            }
        });
    }

    iniciarRotas() {
        let options: LaunchNavigatorOptions = {
            start: this.data.lat + ',' + this.data.lng,

        };

        this.launchNavigator.navigate(this.dados.coleta.endereco_cliente, options)
            .then(
                success => console.log('Launched navigator'),
                error => console.log('Error launching navigator', error)
            );
    }

    chegadaDestino() {
        const alert = this.alertCtrl.create({
            title: 'Parabens!',
            subTitle: 'Você chegou ao local da coleta?',
           
            buttons: [
                {
                    text: 'Sim',
                    handler: () => {
                        this.navCtrl.push(EntregasPage, { dados: this.dados})
                    }
                },
                {
                    text: 'Não',
                    handler: () => {
                        console.log('Agree clicked');
                    }
                }
            ]
        });
        alert.present();
    }


}

