import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColetasPage } from './coletas';

@NgModule({
  declarations: [
    ColetasPage,
  ],
  imports: [
    IonicPageModule.forChild(ColetasPage),
  ],
})
export class ColetasPageModule {}
