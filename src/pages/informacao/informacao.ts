import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Home_02Page } from '../home-02/home-02';

/**
 * Generated class for the InformacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-informacao',
  templateUrl: 'informacao.html',
})
export class InformacaoPage {
  cpf: any;
  device: any;
  show= false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.device = this.navParams.get('device');
  }
  adicionar(value) {
   this.proxima(value)
  }

  proxima(value){
    const config = {deice: this.device, cpf: value};
    this.navCtrl.setRoot(Home_02Page, {'config': config})

  }

  validarCPF(e){
    console.log(e)
    if(e['value'].length == 11){
      this.show = true
    }
  }

}
