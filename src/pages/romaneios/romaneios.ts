import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
//import { ContratoPage } from '../contrato/contrato';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { ContratoPage } from '../contrato/contrato';



@IonicPage()
@Component({
    selector: 'page-romaneios',
    templateUrl: 'romaneios.html',
})
export class RomaneiosPage {
    romaneios: any = [];
    getRomaneios: any = [];
    getEstados:any = [];
    estado:any;
    uf_coleta: any;
    uf_entrega: any;
    relEstados: any=[];
    getCidadesC: any=[];
    getCidadesE: any=[];
    items:any = [];



    constructor(public navCtrl: NavController, public navParams: NavParams, private firebaseProvider: FirebaseProvider, private alertCtrl: AlertController ) {
    }

    ionViewDidLoad() {
        this.getListar();
    }


    async getListar() {
    await  this.firebaseProvider.getAllDoc('romaneios').then((snapshot) => {
        const items = snapshot.val();
        for (const key in items) {
          if(items[key]['coleta']['status'] === "Aberto"){
            items[key].id = key;
            this.getRomaneios.push(items[key])
          } 
        }
      });
      console.log(this.getRomaneios)
    }    
   
    aceitar(data){ 
      data.coleta.status="Em Negociação" 
      this.navCtrl.push(ContratoPage, { dados: data})
    }  

    getItems(ev: any, item) {

        console.log(item)
        
        // Reset items back to all of the items
        this.items;
    
        // set val to the value of the searchbar
        const val = ev.target.value;
    
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.items = this.items.filter((item) => {
            if(item === 'coleta'){
              return (item['coleta']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1 );
            }else {
              return (item['entrega']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1 );
            }
          })
        }
        else{
        // this.initializeItems();
        }
      }
    
    

}
