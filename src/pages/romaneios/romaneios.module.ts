import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RomaneiosPage } from './romaneios';

@NgModule({
  declarations: [
    RomaneiosPage,
  ],
  imports: [
    IonicPageModule.forChild(RomaneiosPage),
  ],
})
export class RomaneiosPageModule {}
