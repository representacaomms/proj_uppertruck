import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Config } from 'ionic-angular';
import { TutorialPage } from '../tutorial/tutorial';
import { HomePage } from '../home/home';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Storage } from '@ionic/storage';

import firebase from 'firebase';
import { Sim } from '@ionic-native/sim';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication';



@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	fretes: any = [];
	status: any;
	dadosLogin: any;
	logar: any;
	data: any = {};
	relMotoristas: any;
	statusLogin: any = false;
	public recaptchaVerifier: firebase.auth.RecaptchaVerifier;

	tabela: string;
	campo: string;
	filtro: string;
	usuario:any;
  device:any;
  show = false;
  show2 = false;


	verificationId: any;
	code: string = "";

	constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, private firebaseProvider: FirebaseProvider,
		public alertCtrl: AlertController, private storage: Storage, private sim: Sim, private firebaseAuthentication: FirebaseAuthentication) {

	}

	ionViewDidLoad() {

		/* const ui = new firebaseui.auth.AuthUI(firebase.auth());
		let config = {
			callbacks: {
				signInSuccessWithAuthResult: function(authResult, redirectUrl) {
				  var user = authResult.user;
				  var credential = authResult.credential;
				  var isNewUser = authResult.additionalUserInfo.isNewUser;
				  var providerId = authResult.additionalUserInfo.providerId;
				  var operationType = authResult.operationType;
				  // Do something with the returned AuthResult.
				  // Return type determines whether we continue the redirect automatically
				  // or whether we leave that to developer to handle.
				  return false;
				},
			},
			signInOptions: [
				firebase.auth.EmailAuthProvider.PROVIDER_ID,
				firebase.auth.GoogleAuthProvider.PROVIDER_ID,
				firebase.auth.FacebookAuthProvider.PROVIDER_ID,
				{
					provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
					defaultCountry: 'BR'
				}
			]
		};

		ui.start('#firebaseui-auth', config); */



    //this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.show = false;
		this.status = 0;
		this.data.cpf = '';
		this.data.password = '';
		this.getDadosCelular();
	}

	authResult(){
		 (authResult)=>{
			console.log("authResult", authResult);
			return true;
		}
	}
	acessar(cpf) {
    this.navCtrl.setRoot(HomePage)
		this.firebaseProvider.getAllDoc('motoristas').then((snapshot) => {
			this.relMotoristas = snapshot.val();
			for (const x in this.relMotoristas) {

				if (this.relMotoristas[x].cpf == cpf ) {
					const motorista = this.relMotoristas[x];
					if(motorista.device == this.device){
						//(44) 9.9737-0229
						const celular = motorista.celular.replace(/\D/g, '');
						this.signIn(celular, motorista)
					}else {
						const prompt = this.alertCtrl.create({
							title: 'Erro',
							message: "Dispositivo não confere com CPF digitado!",

						});
						prompt.present();
					}

				}

			}

		});

	}

	logarPagina() {
		this.status = !this.status;
	}
	cancelar() {
		this.navCtrl.setRoot(LoginPage);
	}
	cadastre() {
		this.storage.set('device', this.device)
		this.navCtrl.setRoot(TutorialPage, { device: this.device})
	}
	esqueceuSenha() {
		const prompt = this.alertCtrl.create({
			title: 'Login',
			message: "Digite seu CPF para verificação",
			inputs: [
				{
					name: 'cpf',
					placeholder: 'CPF'
				},
			],
			buttons: [
				{
					text: 'Cancelar',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Enviar',
					handler: data => {
						this.verificar(data)
					}
				}
			]
		});
		prompt.present();
	}

	verificar(value) {
		this.firebaseProvider.getAllDoc('motoristas').then((snapshot) => {
			this.relMotoristas = snapshot.val();
			for (const x in this.relMotoristas) {
				if (this.relMotoristas[x].cpf == value.cpf) {
					const alert = this.alertCtrl.create({
						title: 'Sua Senha!',
						subTitle: this.relMotoristas[x].cpf,
						buttons: ['OK']
					});
					alert.present();
				}

			}

		});

	}

	mensagemErro() {
		const prompt = this.alertCtrl.create({
			title: 'Login',
			message: "Erro ao Autenticar usuario",

		});
		prompt.present();
	}

	/* signIn(phoneNumber: number, motorista) {
		const appVerifier = this.recaptchaVerifier;
		const phoneNumberString = "+55" + phoneNumber;
		firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
			.then(confirmationResult => {
				// SMS sent. Prompt user to type the code from the message, then sign the
				// user in with confirmationResult.confirm(code).
				let prompt = this.alertCtrl.create({
					title: 'Entre com o codigo',
					inputs: [{ name: 'confirmationCode', placeholder: 'Confirmação do Codigo' }],
					buttons: [
						{
							text: 'Enviar',
							handler: data => {
								if(confirmationResult.verificationId){
									this.encaminhar(motorista);
								}
							}
						}
					]
				});
				prompt.present();
			})
			.catch(function (error) {
				alert(JSON.stringify(error));
			});

	} */

  abrirImput(){
    this.show = !this.show;
  }

	signIn(phoneNumber: number, motorista){
		const phoneNumberString = "+55" + phoneNumber;
		this.firebaseAuthentication.verifyPhoneNumber(phoneNumberString, 60000).then(resultado =>{
			console.log("RESULTADO DO SIGN IF",resultado)
		})
	}
	getDadosCelular() {

		this.sim.hasReadPermission().then(
			(info) => console.log('Has permission: ', info)
		);

		this.sim.requestReadPermission().then(
			() => this.permissaoConcedida(),
			() => console.log('Permission denied')
		);
	}

	permissaoConcedida() {
		this.sim.getSimInfo().then(
			(info) => {
				this.device = info.deviceId
			},
			(err) => console.log('Unable to get sim info: ', err)
		);

	}

	encaminhar(motorista){
		this.storage.set('dados', motorista);
		this.storage.set('Login', 'sim');
		this.navCtrl.setRoot(HomePage)
  }

  validarTelefone(e){
    console.log(e)
    if(e['value'].length == 11){
      this.show2 = true
    }
    if(e['value'].length > 11){
      this.show2 = true
    }
  }




}
// 30546980848
// 358240051111110
