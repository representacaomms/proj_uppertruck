import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { CnhPage } from '../cnh/cnh';
import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions } from "@ionic-native/camera-preview";
import { Home_02Page } from '../home-02/home-02';

@IonicPage()
@Component({
	selector: 'page-selfie',
	templateUrl: 'selfie-foto.html',
})
export class SelfieFotoPage  implements OnInit{
	cpf:any;
	hide:boolean;
	  // picture options
	pictureOpts: CameraPreviewPictureOptions = {
		width: 1280,
		height: 1280,
		quality: 85,

	}

	cameraPreviewOpts: CameraPreviewOptions = {
		x: 0,
		y: 0,
		width: window.screen.width,
		height: window.screen.height,
		camera: 'rear',
		tapPhoto: true,
		previewDrag: true,
		toBack: true,
		alpha: 1

	  };

	constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, private firebaseProvider: FirebaseProvider,
		private alertCtrl: AlertController, private loadingCtrl: LoadingController, private cameraPreview : CameraPreview,) {
			this.cpf = this.navParams.get('cpf')
		}

	ngOnInit(){

    this.hide = true;
		const cameraPreviewOpts: CameraPreviewOptions = {
			x: 0,
			y: 0,
			width: window.screen.width,
			height: window.screen.height,
			camera: 'rear',
			tapPhoto: true,
			previewDrag: true,
			toBack: true,
			alpha: 1
		  };

		  // start camera
		  this.cameraPreview.startCamera(cameraPreviewOpts).then(
			(res) => {
			  console.log(res)

			  this.cameraPreview.switchCamera();

			  this.iniciarCamera()


			},
			(err) => {
			  console.log(err)
			});

  }

	async takePhoto() {
    this.hide=false;
		const result = await this.cameraPreview.takePicture(this.pictureOpts);
		const image = `data:image/jpeg;base64,${result}`;
		this.firebaseProvider.uploadImage(this.cpf, image, 'selfie').then(data => {
			this.proximaPagina(this.cpf);

		}).catch(e => console.error(e))

	}
	proximaPagina(value) {
		const loader = this.loadingCtrl.create({
			content: "Por Favor, Aguarde...",
			duration: 3000
		});
		this.cameraPreview.stopCamera();
    this.navCtrl.popTo(Home_02Page)
		loader.present();
	}

	iniciarCamera(){
			this.cameraPreview.show();
	}


}



