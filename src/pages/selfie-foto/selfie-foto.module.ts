import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelfieFotoPage } from './selfie-foto';

@NgModule({
  declarations: [
    SelfieFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(SelfieFotoPage),
  ],
})
export class SelfieFotoPageModule {}
