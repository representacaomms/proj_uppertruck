import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DocVeiculoPage } from './doc-veiculo';

@NgModule({
  declarations: [
    DocVeiculoPage,
  ],
  imports: [
    IonicPageModule.forChild(DocVeiculoPage),
  ],
})
export class DocVeiculoPageModule {}
