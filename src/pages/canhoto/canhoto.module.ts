import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanhotoPage } from './canhoto';

@NgModule({
  declarations: [
    CanhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(CanhotoPage),
  ],
})
export class CanhotoPageModule {}
