import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HomePage } from '../home/home';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
	selector: 'page-canhoto',
	templateUrl: 'canhoto.html',
})
export class CanhotoPage {
	usuario: any;
	nota: any;

	constructor(private camera: Camera, public navParams: NavParams, public navCtrl: NavController, public firebaseProvider: FirebaseProvider, private loadingCtrl: LoadingController, public alertCtrl: AlertController) {
		this.getUsuario();
	}

	getUsuario() {
		const prompt = this.alertCtrl.create({
			title: 'Prarabéns, Você concluiu seu frete',
			message: "Por favor!, insira o numero da NotaFiscal",
			inputs: [
				{
					name: 'nota',
					placeholder: 'Nota Fiscal'
				},
			],
			buttons: [
				{
					text: 'Cancelar',
					handler: data => {
						this.navCtrl.pop();
					}
				},
				{
					text: 'Salvar',
					handler: data => {
						this.takePhoto(data.nota)
					}
				}
			]
		});
		prompt.present();
	}


	async takePhoto(value) {
		try {
			const options: CameraOptions = {
				quality: 50,
				targetHeight: 600,
				targetWidth: 600,
				destinationType: this.camera.DestinationType.DATA_URL,
				encodingType: this.camera.EncodingType.JPEG,
				mediaType: this.camera.MediaType.PICTURE
			};

			const result = await this.camera.getPicture(options);

			const image = `data:image/jpeg;base64,${result}`;

			this.firebaseProvider.uploadCanhotos(value, image).then(data => {
				this.firebaseProvider.create('canhotos', value);
				console.log('retorno do upload:' + data)
				this.finalizacao();
			}).catch(e => console.error(e))


		} catch (e) {
			console.error(e);
		}
	}
	finalizacao() {
		const prompt = this.alertCtrl.create({
			title: 'Prarabéns, Você concluiu seu frete',
			message: "Deseja tirar foto de outro canhoto?",
			
			buttons: [
				{
					text: 'Não',
					handler: () => {
						this.proximaPagina();
					}
				},
				{
					text: 'Sim',
					handler: () => {
						this.getUsuario()
					}
				}
			]
		});
		prompt.present();
	}
	proximaPagina() {
		const loader = this.loadingCtrl.create({
			content: "Por Favor, Aguarde...",
			duration: 3000
		});
		this.navCtrl.push(HomePage)
		loader.present();
	}

}

