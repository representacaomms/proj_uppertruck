import { FotoVeiculoPage } from './../foto-veiculo/foto-veiculo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelfieFotoPage } from '../selfie-foto/selfie-foto';
import { CnhPage } from '../cnh/cnh';
import { DocVeiculoPage } from '../doc-veiculo/doc-veiculo';

/**
 * Generated class for the Home_02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-02',
  templateUrl: 'home-02.html',
})
export class Home_02Page {
  device:any;
  cpf:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    const config = this.navParams.get('config')
    this.cpf = config.cpf
    this.device = config.device
  }
  selfie(){
    this.navCtrl.push(SelfieFotoPage)
  }
  cnh(){
    this.navCtrl.push(CnhPage)
  }
  crlv(){
    this.navCtrl.push(DocVeiculoPage)
  }
  fotoVeiculo(){
    this.navCtrl.push(FotoVeiculoPage)
  }

}
