import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Home_02Page } from './home-02';

@NgModule({
  declarations: [
    Home_02Page,
  ],
  imports: [
    IonicPageModule.forChild(Home_02Page),
  ],
})
export class Home_02PageModule {}
