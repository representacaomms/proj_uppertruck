import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { TutorialPage } from '../tutorial/tutorial';
import { LoginPage } from '../login/login';


/**
 * Generated class for the ServicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-servicos',
  templateUrl: 'servicos.html',
})
export class ServicosPage implements OnInit {
  servicos: any = [];
  items:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public firebaseProvider: FirebaseProvider, public storage: Storage,
              private alertCtrl: AlertController) {}

  ngOnInit() {

    this.firebaseProvider.getAllDoc('romaneios').then((snapshot) => {
      const items = snapshot.val();
      for (const key in items) {
       this.items.push(items[key])
      }
      console.log(this.items)
    });
   
   
  }

  aceitar(item) {
		const prompt = this.alertCtrl.create({
			title: 'Pré Cadastro',
      message: "Para ver detalhes do frete , você precisa se cadastrar",	
      buttons: [
        {
          text: 'Não sou cadastrado',
					handler: () => {
						this.cadastrar(item)
					}
        },
        {
          text: 'Já tenho cadastro',
					handler: () => {
						this.navCtrl.setRoot(LoginPage)
					}
        }
      ]				
		});
		prompt.present();
	}
  
  cadastrar(item){
    this.navCtrl.push(TutorialPage, { dados: item});
  }
  getItems(ev: any, item) {

    console.log(item)
    
    // Reset items back to all of the items
    this.items;

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        if(item === 'coleta'){
          return (item['coleta']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1 );
        }else {
          return (item['entrega']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1 );
        }
      })
    }
    else{
    // this.initializeItems();
    }
  }

}
