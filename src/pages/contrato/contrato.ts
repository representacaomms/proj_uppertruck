import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ColetasPage } from '../coletas/coletas';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { RomaneiosListarPage } from '../romaneios-listar/romaneios-listar';
//import { ColetasPage } from '../coletas/coletas';
import { Storage } from '@ionic/storage';



@IonicPage()
@Component({
   selector: 'page-contrato',
   templateUrl: 'contrato.html',
})
export class ContratoPage {
   dadosRomaneio: any = {};
   romaneios: any = [];

   constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private firebaseProvider: FirebaseProvider, private storage: Storage) {
      let dados = navParams.get('dados');
      this.dadosRomaneio = dados;
      console.log(this.dadosRomaneio)
   }

   ionViewDidLoad() {
      this.storage.get('romaneios').then((val) => {
         console.log(val)
         if(val){
            this.romaneios = val
         }
       });

   }

   confirmacao() {
      const prompt = this.alertCtrl.create({
         title: 'Concluido!',
         message: "Tem certeza que quer aceitar? ",
         buttons: [
            {
               text: 'Não',
               handler: () => {
                  this.navCtrl.pop();
               }
            },
            {
               text: 'Sim',
               handler: () => {
                  console.log('romaneios', this.dadosRomaneio)
                  this.dadosRomaneio.coleta.status='Fechado';
                  this.romaneios.push(this.dadosRomaneio);
                  
                  this.storage.set('romaneios', this.romaneios)
                  this.firebaseProvider.update('romaneios/', this.dadosRomaneio)
                  this.mensagem(); 
               }
            }
         ]
      });
      prompt.present();
   }

   aceitar() {
      this.navCtrl.setRoot(RomaneiosListarPage)
   }

   novoRomaneio() {
      this.navCtrl.pop()
   }

   mensagem() {
      const prompt = this.alertCtrl.create({
         title: 'Concluido!',
         message: "Deseja outro romaneio? ",
         buttons: [
            {
               text: 'Não',
               handler: () => {
                  this.aceitar();
               }
            },
            {
               text: 'Sim',
               handler: () => {                 
                  this.novoRomaneio();
               }
            }
         ]
      });
      prompt.present();
   }

   recusar(){
      this.navCtrl.pop()
   }

}
