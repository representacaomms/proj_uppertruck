import { Injectable } from '@angular/core';
import { storage, initializeApp, database, auth } from 'firebase';
import { FIREBASE_CONFIG } from '../../app/firebase.config';
import { Storage } from '@ionic/storage';

import { FirebaseAuthentication } from '@ionic-native/firebase-authentication';


@Injectable()
export class FirebaseProvider {
  firebase: any;

  constructor(private storage: Storage, private firebaseAuthentication: FirebaseAuthentication) {
    initializeApp(FIREBASE_CONFIG);
  }

  uploadImage(value, image, local) {
    const pictures = storage().ref('fotos/' + value + '/' + local);
    return pictures.putString(image, 'data_url');
  }

  uploadCanhotos(value, image) {
    const pictures = storage().ref('canhotos/' + value);
    return pictures.putString(image, 'data_url');
  }
  getAllDoc(tabela) {
    const db = database().ref(tabela).once('value');
    return db;

  }

  getDoc(tabela, index) {
    const db = database().ref(tabela + '/' + index).once('value');
    return db;

  }

  create(tabela, data) {
    const create = database().ref().child(tabela + '/').push(data);
    return create
  }

  update(tabela, data) {

    let uid = data.id;
    var updates = {};
    updates[tabela + uid + '/'] = data;

    return database().ref().update(updates);

  }

  atualizarPosicao(value, dados, lat, lng) {
    database().ref('geolocations/' + value).set({
      nome: dados.nome,
      cpf: dados.cpf,
      latitude: lat,
      longitude: lng
    });

  }
  createPosicao(dados, lat, lng) {
    let newData = database().ref('geolocations/').push();
    newData.set({
      nome: dados.nome,
      cpf: dados.cpf,
      latitude: lat,
      longitude: lng
    });
    return newData.key;
  }

  authSMS(number) {
    return this.firebaseAuthentication.verifyPhoneNumber(number, 30000)
  }

  getDadoTabela(tabela, campo, filtro){
    let query = database().ref(tabela).orderByChild(campo).equalTo(filtro);
    return query;
    
  }

  loginWithPhone(verificationID){
    let login = verificationID;
    return true;

  }


}
