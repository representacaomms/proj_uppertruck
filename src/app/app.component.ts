import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { timer } from 'rxjs/observable/timer';
import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';



@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	//showSplash = true; // <-- show animation
	rootPage: any;
	//rootPage:any = HomePage;

	constructor(public platform: Platform, public statusBar: StatusBar,public splashScreen: SplashScreen,  private storage: Storage,
  public alertCtrl: AlertController) {
		this.rootPage = LoginPage;
		setTimeout(() => {
		this.initializeApp();
		}, 600);
		storage.get('Login').then((val) => {
			if (val == 'sim') { // verificando se o existe usuario com base no token, caso sim sera redirecionado para tela de menu
			  this.rootPage = HomePage;
			  setTimeout(() => {
				this.initializeApp();
			  }, 600);
			 
			} else { // caso não sera redirecionado para o login, pois o token não existe ou esta invalido
			  this.rootPage = LoginPage;
			  setTimeout(() => {
				this.initializeApp();
			  }, 600);
			}
		  });         
		
	}

	initializeApp() {
		this.platform.ready().then(() => {

			let funcaoRetorno = (data) => {
				console.log('Notificações: ' + JSON.stringify(data));
		 };
		 window["plugins"].OneSignal.startInit("cf4a2141-103c-4506-99ac-c4ebf7ec1865",
          "785749493075")
          .handleNotificationOpened(funcaoRetorno)
          .endInit();
		 

			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.styleDefault();
			this.splashScreen.show();
			
			timer(3000).subscribe(() => this.splashScreen.hide()); // <-- hide animation after 3s
			//this.pushsetup();

			 // statusBar.overlaysWebView(true);
			 this.statusBar.backgroundColorByHexString('#00796B')
			 // statusBar.overlaysWebView(true);
			 this.statusBar.backgroundColorByHexString('#00796B') 

		});           
	}
	
}




 

