import { InformacaoPageModule } from './../pages/informacao/informacao.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Camera } from '@ionic-native/camera';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { SelfiePageModule } from '../pages/selfie/selfie.module';
import { CnhPageModule } from '../pages/cnh/cnh.module';
import { LoginPageModule } from '../pages/login/login.module';
import { FotoVeiculoPageModule } from '../pages/foto-veiculo/foto-veiculo.module';
import { DocVeiculoPageModule } from '../pages/doc-veiculo/doc-veiculo.module';
import { TutorialPageModule } from '../pages/tutorial/tutorial.module';
import { IonicStorageModule } from '@ionic/storage';
import { ServicosPageModule } from '../pages/servicos/servicos.module';
import { RomaneiosPageModule } from '../pages/romaneios/romaneios.module';
import { ContratoPageModule } from '../pages/contrato/contrato.module';
import { ColetasPageModule } from '../pages/coletas/coletas.module';
import { EntregasPageModule } from '../pages/entregas/entregas.module';
import { CanhotoPageModule } from '../pages/canhoto/canhoto.module';
import { Geolocation } from '@ionic-native/geolocation';

import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { RomaneiosListarPageModule } from '../pages/romaneios-listar/romaneios-listar.module';
import { CameraPreview } from '@ionic-native/camera-preview';

import { FirebaseAuthentication } from '@ionic-native/firebase-authentication';
import { Sim } from '@ionic-native/sim';
import { SelfieFotoPageModule } from '../pages/selfie-foto/selfie-foto.module';
import { Home_02PageModule } from '../pages/home-02/home-02.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    LoginPageModule,
    FotoVeiculoPageModule,
    DocVeiculoPageModule,
    SelfiePageModule,
    CnhPageModule,
    TutorialPageModule,
    ServicosPageModule,
    RomaneiosPageModule,
    ContratoPageModule,
    ColetasPageModule,
    EntregasPageModule,
    CanhotoPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      driverOrder:['localstorage']
  }),
    RomaneiosListarPageModule,
    SelfieFotoPageModule,
    Home_02PageModule,
    InformacaoPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    FirebaseProvider,
    Geolocation,
    BackgroundGeolocation,
    LaunchNavigator,
    CameraPreview,
    FirebaseAuthentication,
    Sim

  ]
})
export class AppModule {}
