webpackJsonp([1],{

/***/ 127:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 127;

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InformacaoPageModule", function() { return InformacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__informacao__ = __webpack_require__(183);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InformacaoPageModule = /** @class */ (function () {
    function InformacaoPageModule() {
    }
    InformacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__informacao__["a" /* InformacaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__informacao__["a" /* InformacaoPage */]),
            ],
        })
    ], InformacaoPageModule);
    return InformacaoPageModule;
}());

//# sourceMappingURL=informacao.module.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_firebase_config__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_firebase_authentication__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FirebaseProvider = /** @class */ (function () {
    function FirebaseProvider(storage, firebaseAuthentication) {
        this.storage = storage;
        this.firebaseAuthentication = firebaseAuthentication;
        Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["initializeApp"])(__WEBPACK_IMPORTED_MODULE_2__app_firebase_config__["a" /* FIREBASE_CONFIG */]);
    }
    FirebaseProvider.prototype.uploadImage = function (value, image, local) {
        var pictures = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["storage"])().ref('fotos/' + value + '/' + local);
        return pictures.putString(image, 'data_url');
    };
    FirebaseProvider.prototype.uploadCanhotos = function (value, image) {
        var pictures = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["storage"])().ref('canhotos/' + value);
        return pictures.putString(image, 'data_url');
    };
    FirebaseProvider.prototype.getAllDoc = function (tabela) {
        var db = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref(tabela).once('value');
        return db;
    };
    FirebaseProvider.prototype.getDoc = function (tabela, index) {
        var db = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref(tabela + '/' + index).once('value');
        return db;
    };
    FirebaseProvider.prototype.create = function (tabela, data) {
        var create = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref().child(tabela + '/').push(data);
        return create;
    };
    FirebaseProvider.prototype.update = function (tabela, data) {
        var uid = data.id;
        var updates = {};
        updates[tabela + uid + '/'] = data;
        return Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref().update(updates);
    };
    FirebaseProvider.prototype.atualizarPosicao = function (value, dados, lat, lng) {
        Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref('geolocations/' + value).set({
            nome: dados.nome,
            cpf: dados.cpf,
            latitude: lat,
            longitude: lng
        });
    };
    FirebaseProvider.prototype.createPosicao = function (dados, lat, lng) {
        var newData = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref('geolocations/').push();
        newData.set({
            nome: dados.nome,
            cpf: dados.cpf,
            latitude: lat,
            longitude: lng
        });
        return newData.key;
    };
    FirebaseProvider.prototype.authSMS = function (number) {
        return this.firebaseAuthentication.verifyPhoneNumber(number, 30000);
    };
    FirebaseProvider.prototype.getDadoTabela = function (tabela, campo, filtro) {
        var query = Object(__WEBPACK_IMPORTED_MODULE_1_firebase__["database"])().ref(tabela).orderByChild(campo).equalTo(filtro);
        return query;
    };
    FirebaseProvider.prototype.loginWithPhone = function (verificationID) {
        var login = verificationID;
        return true;
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_firebase_authentication__["a" /* FirebaseAuthentication */]])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/canhoto/canhoto.module": [
		170
	],
	"../pages/cnh/cnh.module": [
		181
	],
	"../pages/coletas/coletas.module": [
		197
	],
	"../pages/contrato/contrato.module": [
		186
	],
	"../pages/doc-veiculo/doc-veiculo.module": [
		185
	],
	"../pages/entregas/entregas.module": [
		187
	],
	"../pages/espaco-veiculo/espaco-veiculo.module": [
		338,
		0
	],
	"../pages/foto-veiculo/foto-veiculo.module": [
		188
	],
	"../pages/home-02/home-02.module": [
		189
	],
	"../pages/informacao/informacao.module": [
		128
	],
	"../pages/login/login.module": [
		190
	],
	"../pages/romaneios-listar/romaneios-listar.module": [
		191
	],
	"../pages/romaneios/romaneios.module": [
		192
	],
	"../pages/selfie-foto/selfie-foto.module": [
		193
	],
	"../pages/selfie/selfie.module": [
		194
	],
	"../pages/servicos/servicos.module": [
		196
	],
	"../pages/tutorial/tutorial.module": [
		195
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 169;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanhotoPageModule", function() { return CanhotoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__canhoto__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CanhotoPageModule = /** @class */ (function () {
    function CanhotoPageModule() {
    }
    CanhotoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__canhoto__["a" /* CanhotoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__canhoto__["a" /* CanhotoPage */]),
            ],
        })
    ], CanhotoPageModule);
    return CanhotoPageModule;
}());

//# sourceMappingURL=canhoto.module.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CanhotoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var CanhotoPage = /** @class */ (function () {
    function CanhotoPage(camera, navParams, navCtrl, firebaseProvider, loadingCtrl, alertCtrl) {
        this.camera = camera;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.firebaseProvider = firebaseProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.getUsuario();
    }
    CanhotoPage.prototype.getUsuario = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Prarabéns, Você concluiu seu frete',
            message: "Por favor!, insira o numero da NotaFiscal",
            inputs: [
                {
                    name: 'nota',
                    placeholder: 'Nota Fiscal'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function (data) {
                        _this.navCtrl.pop();
                    }
                },
                {
                    text: 'Salvar',
                    handler: function (data) {
                        _this.takePhoto(data.nota);
                    }
                }
            ]
        });
        prompt.present();
    };
    CanhotoPage.prototype.takePhoto = function (value) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, result, image, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            quality: 50,
                            targetHeight: 600,
                            targetWidth: 600,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE
                        };
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        image = "data:image/jpeg;base64," + result;
                        this.firebaseProvider.uploadCanhotos(value, image).then(function (data) {
                            _this.firebaseProvider.create('canhotos', value);
                            console.log('retorno do upload:' + data);
                            _this.finalizacao();
                        }).catch(function (e) { return console.error(e); });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CanhotoPage.prototype.finalizacao = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Prarabéns, Você concluiu seu frete',
            message: "Deseja tirar foto de outro canhoto?",
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        _this.proximaPagina();
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.getUsuario();
                    }
                }
            ]
        });
        prompt.present();
    };
    CanhotoPage.prototype.proximaPagina = function () {
        var loader = this.loadingCtrl.create({
            content: "Por Favor, Aguarde...",
            duration: 3000
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        loader.present();
    };
    CanhotoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-canhoto',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/canhoto/canhoto.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n      <ion-title>Canhoto Nota Fiscal</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  \n  <ion-content padding>\n    <ion-row>\n      <ion-col col-12 class="dicas" text-center>\n        <h2>Para concluir o frete tire uma foto do canhoto da nota !</h2>\n      </ion-col>\n    </ion-row>\n    <hr />\n    <ion-row class="conteudo">\n      <ion-col col-12 text-center>\n        <h1>Selfie</h1>\n        <p class="dicas">Faça uma Foto do canhoto!</p>\n      </ion-col>\n    </ion-row>   \n    <ion-row >\n      <button ion-button round  full (click)="takePhoto()" > Tirar Foto </button>\n    </ion-row>\n   \n   \n  \n  </ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/canhoto/canhoto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], CanhotoPage);
    return CanhotoPage;
}());

//# sourceMappingURL=canhoto.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RomaneiosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contrato_contrato__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


//import { ContratoPage } from '../contrato/contrato';


var RomaneiosPage = /** @class */ (function () {
    function RomaneiosPage(navCtrl, navParams, firebaseProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.romaneios = [];
        this.getRomaneios = [];
        this.getEstados = [];
        this.relEstados = [];
        this.getCidadesC = [];
        this.getCidadesE = [];
        this.items = [];
    }
    RomaneiosPage.prototype.ionViewDidLoad = function () {
        this.getListar();
    };
    RomaneiosPage.prototype.getListar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebaseProvider.getAllDoc('romaneios').then(function (snapshot) {
                            var items = snapshot.val();
                            for (var key in items) {
                                if (items[key]['coleta']['status'] === "Aberto") {
                                    items[key].id = key;
                                    _this.getRomaneios.push(items[key]);
                                }
                            }
                        })];
                    case 1:
                        _a.sent();
                        console.log(this.getRomaneios);
                        return [2 /*return*/];
                }
            });
        });
    };
    RomaneiosPage.prototype.aceitar = function (data) {
        data.coleta.status = "Em Negociação";
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contrato_contrato__["a" /* ContratoPage */], { dados: data });
    };
    RomaneiosPage.prototype.getItems = function (ev, item) {
        console.log(item);
        // Reset items back to all of the items
        this.items;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                if (item === 'coleta') {
                    return (item['coleta']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1);
                }
                else {
                    return (item['entrega']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1);
                }
            });
        }
        else {
            // this.initializeItems();
        }
    };
    RomaneiosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-romaneios',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/romaneios/romaneios.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Romaneios</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n      <ion-searchbar (ionInput)="getItems($event, \'coleta\' )" placeholder="Coleta UF "></ion-searchbar>\n      <ion-searchbar (ionInput)="getItems($event, \'entrega\' )" placeholder="Entrega UF "></ion-searchbar>\n    </ion-card-content>\n  </ion-card>\n  <!-- ;[hidden]="telefone.get(\'apagar\').value" -->\n  <ion-card *ngFor="let item of getRomaneios">\n    <ion-card-content>\n      <ion-row no-padding>\n        <ion-card-title>\n          <ion-col col-7 text-rigth>\n            <b>Coleta: </b> {{item.coleta.cidade_cliente}} / {{ item.coleta.uf_cliente }}\n          </ion-col>\n        </ion-card-title>\n      </ion-row>\n      <ion-row no-padding>\n        <ion-card-title>\n          <ion-col col-7 text-rigth>\n            <b>Entrega:</b> {{item.entrega.cidade_cliente}} / {{ item.entrega.uf_cliente }}\n          </ion-col>\n        </ion-card-title>\n      </ion-row>\n      <hr />\n      <ion-row no-padding>\n        <ion-col col-7 text-rigth>\n          <b class="resaltar">R$ Frete:</b>{{item.coleta.frete_motorista }}\n        </ion-col>\n      </ion-row>\n      <hr />\n\n\n\n    </ion-card-content>\n    <ion-row no-padding>\n      <ion-col col-12 item-end>\n        <button ion-button round full small color="primary" (click)="aceitar(item)">\n          Aceitar\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/romaneios/romaneios.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], RomaneiosPage);
    return RomaneiosPage;
}());

//# sourceMappingURL=romaneios.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContratoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__romaneios_listar_romaneios_listar__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { ColetasPage } from '../coletas/coletas';

var ContratoPage = /** @class */ (function () {
    function ContratoPage(navCtrl, navParams, alertCtrl, firebaseProvider, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.firebaseProvider = firebaseProvider;
        this.storage = storage;
        this.dadosRomaneio = {};
        this.romaneios = [];
        var dados = navParams.get('dados');
        this.dadosRomaneio = dados;
        console.log(this.dadosRomaneio);
    }
    ContratoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('romaneios').then(function (val) {
            console.log(val);
            if (val) {
                _this.romaneios = val;
            }
        });
    };
    ContratoPage.prototype.confirmacao = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Concluido!',
            message: "Tem certeza que quer aceitar? ",
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        _this.navCtrl.pop();
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        console.log('romaneios', _this.dadosRomaneio);
                        _this.dadosRomaneio.coleta.status = 'Fechado';
                        _this.romaneios.push(_this.dadosRomaneio);
                        _this.storage.set('romaneios', _this.romaneios);
                        _this.firebaseProvider.update('romaneios/', _this.dadosRomaneio);
                        _this.mensagem();
                    }
                }
            ]
        });
        prompt.present();
    };
    ContratoPage.prototype.aceitar = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__romaneios_listar_romaneios_listar__["a" /* RomaneiosListarPage */]);
    };
    ContratoPage.prototype.novoRomaneio = function () {
        this.navCtrl.pop();
    };
    ContratoPage.prototype.mensagem = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Concluido!',
            message: "Deseja outro romaneio? ",
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        _this.aceitar();
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.novoRomaneio();
                    }
                }
            ]
        });
        prompt.present();
    };
    ContratoPage.prototype.recusar = function () {
        this.navCtrl.pop();
    };
    ContratoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contrato',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/contrato/contrato.html"*/'<!--\n  Generated template for the ContratoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Contrato</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n   <h4>PREZADO CONDUTOR LEIA COM ATENÇÃO:</h4> \n   <h6> ** Concordando com as informações abaixo, automaticamente seu app Uppertruck deverá permanecer no modo “Segundo Plano” até o fim da operação. A não observância poderá acarretar em bloqueio de pagamentos ou cancelamento de bônus**</h6>\n    \n    \n    \n   <p><b>A) Percurso:</b></p>\n    \n   <p><b> - Coleta:</b></p>\n   <p><b>Cidade: </b> {{this.dadosRomaneio.coleta.cidade_cliente}} / {{this.dadosRomaneio.coleta.uf_cliente}}</p>\n   <p><b>Data: </b>{{this.dadosRomaneio.coleta.data}}</p>    \n   <hr>\n   <p><b> - Entrega:</b></p>\n   <p><b>Cidade: </b> {{this.dadosRomaneio.entrega.cidade_cliente}} /  {{this.dadosRomaneio.entrega.uf_cliente}}</p>\n   <p><b>Data: </b>{{this.dadosRomaneio.entrega.data}}</p>    \n   <hr>\n    \n   <p><b> B) Embarque e Desembarque:</b></p>\n   <p>  - Tempo estimado de carga/descarga: 2h</p>\n   <p> - Não há custos com o serviço </p> \n    \n   <p><b> C) Itens Obrigatórios:</b></p>\n   <p>  - Calça, Camisa/camiseta, Calçado fechado </p>\n   <p>  - Cintas/Cordas/Catracas para aperto da carga /p> \n\n   <p><b> C) Itens Obrigatórios:</b></p>\n   <p> - Calça, Camisa/camiseta, Calçado fechado </p>\n   <p>  - Cintas/Cordas/Catracas para aperto da carga</p> \n\n\n   <p><b>  D) Documentação:</b></p>\n   <p>  - CNH, Documentos do veículo, ANTT</p>\n   \n   <p><b>  F) Material transportado:</b></p>\n   <p><b>Produto: </b>{{this.dadosRomaneio.coleta.data}}</p>   \n   <p><b>Qtd/Vol: </b>{{this.dadosRomaneio.coleta.qtde}}</p>   \n   <p><b>Descricao: </b>{{this.dadosRomaneio.coleta.descricao}}</p>   \n   <p><b>Medida unitária: </b>{{this.dadosRomaneio.coleta.medida}}</p>   \n   <p><b>Peso Total: </b>{{this.dadosRomaneio.coleta.peso}}</p>   \n   \n   <p><b>  G) Pagamento da Prestação de serviço:</b></p>\n   <p> - Conta corrente/ poupança indicada pelo *proprietário* e com apontamento do CIOT da ANTT.</p>  \n   <p><b>Valor Total: </b>{{this.dadosRomaneio.coleta.frete_motorista}}</p>   \n   <p><b>Adiantamento: </b>100%</p>   \n   <p><b>Saldo: </b>0% na entrega</p>   \n   <p><b>Envio de canhoto via app </b></p>   \n \n   <p><b>   O) Orientações Finais:</b></p>\n   <p> - Conduzir seu veículo com responsabilidade</p>  \n   <p> - Comunicar previamente alteração de rota.</p>  \n   <p> - Parar somente em locais seguros .</p>  \n   <p> - Pagamento total do transporte condicionado à entrega do material em sua totalidade mediante nota fiscal.</p> \n\n    <ion-row class="button">\n    <button ion-button round  full  color="danger" (click)="recusar()" > Recusar </button>\n    <button ion-button round  full (click)="confirmacao()" > Aceitar </button>\n  </ion-row>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/contrato/contrato.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], ContratoPage);
    return ContratoPage;
}());

//# sourceMappingURL=contrato.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RomaneiosListarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__coletas_coletas__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RomaneiosListarPage = /** @class */ (function () {
    function RomaneiosListarPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.getRomaneios = [];
    }
    RomaneiosListarPage.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get('romaneios').then(function (val) {
            for (var key in val) {
                var element = val[key];
                if (element.coleta.status == "Fechado") {
                    _this.getRomaneios.push(element);
                }
            }
        });
        setTimeout(function () {
            console.log('Romaneios: ', _this.getRomaneios);
        }, 500);
    };
    RomaneiosListarPage.prototype.selecionar = function (value) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__coletas_coletas__["a" /* ColetasPage */], { dados: value });
    };
    RomaneiosListarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-romaneios-listar',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/romaneios-listar/romaneios-listar.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n    </button>\n    <ion-title>Lista Romaneios</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list inset>\n    <ion-item *ngFor="let item of getRomaneios;" (click)="selecionar(item)">\n      <ion-card>\n        <ion-card-header>\n          <b>Coleta</b>\n          <h5>{{item.coleta.cidade_cliente}}</h5>\n        </ion-card-header>\n        <ion-card-header>\n          <b>Entrega</b>\n          <h5>{{item.entrega.cidade_cliente}}</h5>\n        </ion-card-header>\n      </ion-card>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/romaneios-listar/romaneios-listar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], RomaneiosListarPage);
    return RomaneiosListarPage;
}());

//# sourceMappingURL=romaneios-listar.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ColetasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_launch_navigator__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__entregas_entregas__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker } from '@ionic-native/google-maps';





var ColetasPage = /** @class */ (function () {
    function ColetasPage(navCtrl, navParams, /* public service: ServiceProvider, */ storage, http, launchNavigator, alertCtrl, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.launchNavigator = launchNavigator;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.item = {};
        this.dados = {};
        this.data = {};
        this.coletas = {};
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.coords = {};
        var dados = this.navParams.get('dados');
        this.dados = dados;
        this.cpf = dados.cpf;
        console.log(this.dados);
    }
    ColetasPage.prototype.ionViewDidLoad = function () {
        this.initializeMap();
    };
    ColetasPage.prototype.initializeMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.startPosition = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            console.log('dentro do mapa' + resp.coords.latitude + resp.coords.longitude);
            _this.data.lat = resp.coords.latitude;
            _this.data.lng = resp.coords.longitude;
            var mapOptions = {
                zoom: 18,
                center: _this.startPosition,
                styles: [
                    { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
                    {
                        featureType: 'administrative.locality',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [{ color: '#263c3f' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#6b9a76' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [{ color: '#38414e' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#212a37' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#9ca5b3' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [{ color: '#746855' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#1f2835' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#f3d19c' }]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'geometry',
                        stylers: [{ color: '#2f3948' }]
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [{ color: '#17263c' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#515c6d' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.stroke',
                        stylers: [{ color: '#17263c' }]
                    }
                ]
            };
            _this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            _this.directionsDisplay.setMap(_this.map);
            var marker = new google.maps.Marker({
                position: _this.startPosition,
                map: _this.map,
            });
            setTimeout(function () {
                _this.calculateRoute(resp.coords.latitude, resp.coords.longitude);
                _this.calcularDistancia(resp.coords.latitude, resp.coords.longitude);
            }, 1000);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    ColetasPage.prototype.calcularDistancia = function (lat, lng) {
        var _this = this;
        this.http.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + lat + "," + lng + "&destinations=" + this.dados.coleta.endereco_cliente + "&key=AIzaSyAt9KtdFvn2WCY7xo_Jf9IkjNLIXh1gSvQ")
            .subscribe(function (data) {
            console.log(data);
            var dados = JSON.parse(data['_body']);
            _this.distancia = dados['rows'][0]['elements'][0]['distance'].text;
            _this.tempo = dados['rows'][0]['elements'][0]['duration'].text;
        });
    };
    ColetasPage.prototype.calculateRoute = function (lat, lng) {
        var request = {
            // Pode ser uma coordenada (LatLng), uma string ou um lugar
            origin: this.startPosition,
            destination: this.dados.coleta.endereco_cliente,
            travelMode: 'DRIVING'
        };
        this.traceRoute(this.directionsService, this.directionsDisplay, request);
    };
    ColetasPage.prototype.traceRoute = function (service, display, request) {
        service.route(request, function (result, status) {
            if (status == 'OK') {
                display.setDirections(result);
            }
        });
    };
    ColetasPage.prototype.iniciarRotas = function () {
        var options = {
            start: this.data.lat + ',' + this.data.lng,
        };
        this.launchNavigator.navigate(this.dados.coleta.endereco_cliente, options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    ColetasPage.prototype.chegadaDestino = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Parabens!',
            subTitle: 'Você chegou ao local da coleta?',
            buttons: [
                {
                    text: 'Sim',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__entregas_entregas__["a" /* EntregasPage */], { dados: _this.dados });
                    }
                },
                {
                    text: 'Não',
                    handler: function () {
                        console.log('Agree clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    ColetasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-coletas',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/coletas/coletas.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Coletas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n    <button ion-button icon-left full text-center (click)="chegadaDestino()"> Clique quando chegar no destino!</button>\n  </div>\n  <div id="map"></div>\n  <ion-item>\n    <ion-icon name="home" item-left large></ion-icon>\n    <h2>{{ this.dados.coleta.razao_social_cliente }}</h2>\n    <p>{{ this.dados.coleta.endereco_cliente }}</p>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="information-circle" item-left large></ion-icon>\n    <h2> Tel- Cliente: {{ this.dados.coleta.telefone_cliente }}</h2>\n    <p class="resaltar"> R$ Frete: {{ this.dados.coleta.frete_motorista  }}</p>\n  </ion-item>\n\n  <ion-item>\n    <span item-left>{{ distancia }}</span>\n    <span item-left>{{ tempo }}</span>\n    <button ion-button icon-left clear item-end (click)="iniciarRotas()">\n      <ion-icon name="navigate"></ion-icon>\n      Start\n    </button>\n  </ion-item>\n\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/coletas/coletas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */]])
    ], ColetasPage);
    return ColetasPage;
}());

//# sourceMappingURL=coletas.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntregasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__canhoto_canhoto__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_launch_navigator__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EntregasPage = /** @class */ (function () {
    function EntregasPage(navCtrl, navParams, /*  public service: ServiceProvider, */ storage, http, launchNavigator, alertCtrl, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.launchNavigator = launchNavigator;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.item = {};
        this.dados = {};
        this.data = {};
        this.entregas = {};
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.coords = {};
        var dados = this.navParams.get('dados');
        this.dados = dados;
    }
    EntregasPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log(this.dados);
        setTimeout(function () {
            _this.initializeMap();
        }, 1000);
    };
    EntregasPage.prototype.initializeMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.startPosition = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            _this.data.lat = resp.coords.latitude;
            _this.data.lng = resp.coords.longitude;
            var mapOptions = {
                zoom: 18,
                center: _this.startPosition,
                styles: [
                    { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
                    { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
                    {
                        featureType: 'administrative.locality',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [{ color: '#263c3f' }]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#6b9a76' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [{ color: '#38414e' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#212a37' }]
                    },
                    {
                        featureType: 'road',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#9ca5b3' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [{ color: '#746855' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{ color: '#1f2835' }]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#f3d19c' }]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'geometry',
                        stylers: [{ color: '#2f3948' }]
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#d59563' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [{ color: '#17263c' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [{ color: '#515c6d' }]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.stroke',
                        stylers: [{ color: '#17263c' }]
                    }
                ]
            };
            _this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
            _this.directionsDisplay.setMap(_this.map);
            var marker = new google.maps.Marker({
                position: _this.startPosition,
                map: _this.map,
            });
            setTimeout(function () {
                _this.calculateRoute(resp.coords.latitude, resp.coords.longitude);
                _this.calcularDistancia(resp.coords.latitude, resp.coords.longitude);
            }, 1000);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    EntregasPage.prototype.calcularDistancia = function (lat, lng) {
        var _this = this;
        this.http.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + lat + "," + lng + "&destinations=" + this.dados.entrega.endereco_cliente + "&key=AIzaSyAt9KtdFvn2WCY7xo_Jf9IkjNLIXh1gSvQ")
            .subscribe(function (data) {
            var dados = JSON.parse(data['_body']);
            _this.distancia = dados['rows'][0]['elements'][0]['distance'].text;
            _this.tempo = dados['rows'][0]['elements'][0]['duration'].text;
        });
    };
    EntregasPage.prototype.calculateRoute = function (lat, lng) {
        var request = {
            // Pode ser uma coordenada (LatLng), uma string ou um lugar
            origin: this.startPosition,
            destination: this.dados.endereco_cliente,
            travelMode: 'DRIVING'
        };
        this.traceRoute(this.directionsService, this.directionsDisplay, request);
    };
    EntregasPage.prototype.traceRoute = function (service, display, request) {
        service.route(request, function (result, status) {
            if (status == 'OK') {
                display.setDirections(result);
            }
        });
    };
    EntregasPage.prototype.iniciarRotas = function () {
        var options = {
            start: this.data.lat + ',' + this.data.lng,
        };
        this.launchNavigator.navigate(this.dados.endereco_cliente, options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    EntregasPage.prototype.chegadaDestino = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Parabens!',
            subTitle: 'Você chegou ao seu destino?',
            buttons: [
                {
                    text: 'Sim',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__canhoto_canhoto__["a" /* CanhotoPage */]);
                    }
                },
                {
                    text: 'Não',
                    handler: function () {
                        console.log('Agree clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    EntregasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-entregas',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/entregas/entregas.html"*/'<ion-header>\n\n	<ion-navbar color="primary">\n		<ion-title>Entregas</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<div>\n		<button ion-button icon-left full text-center  (click)="chegadaDestino()" > Clique quando chegar no destino!</button>\n	</div>\n	<div id="map"></div>\n	<ion-item>\n		<ion-icon name="home" item-left large></ion-icon>\n		<h2>{{ this.dados.entrega.razao_social_cliente }}</h2>\n   		<p>{{ this.dados.entrega.endereco_cliente }}</p>\n	</ion-item>\n\n	<ion-item>\n		<ion-icon name="information-circle" item-left large></ion-icon>\n		<h2> Tel- Cliente: {{ this.dados.entrega.telefone_cliente }}</h2>\n    	<p class="resaltar"> R$ Frete: {{ this.dados.coleta.frete_motorista }}</p>\n	</ion-item>\n\n	<ion-item>\n		<span item-left>{{ distancia }}</span>\n		<span item-left>{{ tempo }}</span>\n		<button ion-button icon-left clear item-end (click)="iniciarRotas()">\n			<ion-icon name="navigate"></ion-icon>\n			Start\n		</button>\n	</ion-item>\n\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/entregas/entregas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */]])
    ], EntregasPage);
    return EntregasPage;
}());

//# sourceMappingURL=entregas.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CnhPageModule", function() { return CnhPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cnh__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CnhPageModule = /** @class */ (function () {
    function CnhPageModule() {
    }
    CnhPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cnh__["a" /* CnhPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cnh__["a" /* CnhPage */]),
            ],
        })
    ], CnhPageModule);
    return CnhPageModule;
}());

//# sourceMappingURL=cnh.module.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CnhPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__doc_veiculo_doc_veiculo__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var CnhPage = /** @class */ (function () {
    function CnhPage(navCtrl, navParams, camera, firebaseProvider, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    CnhPage.prototype.takePhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, result, image, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            quality: 50,
                            targetHeight: 600,
                            targetWidth: 600,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            cameraDirection: 0,
                            correctOrientation: true
                        };
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        image = "data:image/jpeg;base64," + result;
                        this.firebaseProvider.uploadImage(this.navParams.get('dados'), image, 'cnh').then(function (data) {
                            _this.proximaPagina(_this.navParams.get('dados'));
                        }).catch(function (e) { return console.error(e); });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CnhPage.prototype.proximaPagina = function (value) {
        var loader = this.loadingCtrl.create({
            content: "Por Favor, Aguarde...",
            duration: 3000
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__doc_veiculo_doc_veiculo__["a" /* DocVeiculoPage */], { dados: value });
        loader.present();
    };
    CnhPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cnh',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/cnh/cnh.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>CNH</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 class="dicas" text-center>\n        <h2>Scaneando seus documentos !</h2>\n      </ion-col>\n    </ion-row>\n    <hr />\n    <ion-row class="conteudo">\n      <ion-col col-12 text-center >\n        <h1>CNH</h1>\n        <p class="dicas">Faça uma foto da sua CNH!</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-row class="button">\n    <button ion-button round  full (click)="takePhoto()" > Tirar Foto </button>\n  </ion-row>\n<!--   <ion-img width="280" height="280" src="assets/imgs/CNH.png" ></ion-img>\n -->\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/cnh/cnh.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], CnhPage);
    return CnhPage;
}());

//# sourceMappingURL=cnh.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_02_home_02__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InformacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InformacaoPage = /** @class */ (function () {
    function InformacaoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.show = false;
    }
    InformacaoPage.prototype.ionViewDidLoad = function () {
        this.device = this.navParams.get('device');
    };
    InformacaoPage.prototype.adicionar = function (value) {
        this.proxima(value);
    };
    InformacaoPage.prototype.proxima = function (value) {
        var config = { deice: this.device, cpf: value };
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_02_home_02__["a" /* Home_02Page */], { 'config': config });
    };
    InformacaoPage.prototype.validarCPF = function (e) {
        console.log(e);
        if (e['value'].length == 11) {
            this.show = true;
        }
    };
    InformacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-informacao',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/informacao/informacao.html"*/'<ion-header>\n\n  </ion-header>\n\n\n  <ion-content padding>\n    <h4>DIGITE seu CPF e clique em PROXIMO!</h4>\n    <hr />\n      <ion-item>\n        <ion-label floating>Digite seu CPF</ion-label>\n        <ion-input [(ngModel)]="cpf" type="number" (ionChange)="validarCPF($event)"></ion-input>\n      </ion-item>\n      <div *ngIf="show">\n          <button ion-button round block color="primary" (click)="adicionar(cpf)"> Proximo </button>\n      </div>\n  </ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/informacao/informacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], InformacaoPage);
    return InformacaoPage;
}());

//# sourceMappingURL=informacao.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocVeiculoPageModule", function() { return DocVeiculoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__doc_veiculo__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DocVeiculoPageModule = /** @class */ (function () {
    function DocVeiculoPageModule() {
    }
    DocVeiculoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__doc_veiculo__["a" /* DocVeiculoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__doc_veiculo__["a" /* DocVeiculoPage */]),
            ],
        })
    ], DocVeiculoPageModule);
    return DocVeiculoPageModule;
}());

//# sourceMappingURL=doc-veiculo.module.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContratoPageModule", function() { return ContratoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contrato__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContratoPageModule = /** @class */ (function () {
    function ContratoPageModule() {
    }
    ContratoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contrato__["a" /* ContratoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contrato__["a" /* ContratoPage */]),
            ],
        })
    ], ContratoPageModule);
    return ContratoPageModule;
}());

//# sourceMappingURL=contrato.module.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntregasPageModule", function() { return EntregasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entregas__ = __webpack_require__(179);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EntregasPageModule = /** @class */ (function () {
    function EntregasPageModule() {
    }
    EntregasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__entregas__["a" /* EntregasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__entregas__["a" /* EntregasPage */]),
            ],
        })
    ], EntregasPageModule);
    return EntregasPageModule;
}());

//# sourceMappingURL=entregas.module.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FotoVeiculoPageModule", function() { return FotoVeiculoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__foto_veiculo__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FotoVeiculoPageModule = /** @class */ (function () {
    function FotoVeiculoPageModule() {
    }
    FotoVeiculoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__foto_veiculo__["a" /* FotoVeiculoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__foto_veiculo__["a" /* FotoVeiculoPage */]),
            ],
        })
    ], FotoVeiculoPageModule);
    return FotoVeiculoPageModule;
}());

//# sourceMappingURL=foto-veiculo.module.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home_02PageModule", function() { return Home_02PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_02__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Home_02PageModule = /** @class */ (function () {
    function Home_02PageModule() {
    }
    Home_02PageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home_02__["a" /* Home_02Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home_02__["a" /* Home_02Page */]),
            ],
        })
    ], Home_02PageModule);
    return Home_02PageModule;
}());

//# sourceMappingURL=home-02.module.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RomaneiosListarPageModule", function() { return RomaneiosListarPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__romaneios_listar__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RomaneiosListarPageModule = /** @class */ (function () {
    function RomaneiosListarPageModule() {
    }
    RomaneiosListarPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__romaneios_listar__["a" /* RomaneiosListarPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__romaneios_listar__["a" /* RomaneiosListarPage */]),
            ],
        })
    ], RomaneiosListarPageModule);
    return RomaneiosListarPageModule;
}());

//# sourceMappingURL=romaneios-listar.module.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RomaneiosPageModule", function() { return RomaneiosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__romaneios__ = __webpack_require__(173);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RomaneiosPageModule = /** @class */ (function () {
    function RomaneiosPageModule() {
    }
    RomaneiosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__romaneios__["a" /* RomaneiosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__romaneios__["a" /* RomaneiosPage */]),
            ],
        })
    ], RomaneiosPageModule);
    return RomaneiosPageModule;
}());

//# sourceMappingURL=romaneios.module.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfieFotoPageModule", function() { return SelfieFotoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__selfie_foto__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SelfieFotoPageModule = /** @class */ (function () {
    function SelfieFotoPageModule() {
    }
    SelfieFotoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__selfie_foto__["a" /* SelfieFotoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__selfie_foto__["a" /* SelfieFotoPage */]),
            ],
        })
    ], SelfieFotoPageModule);
    return SelfieFotoPageModule;
}());

//# sourceMappingURL=selfie-foto.module.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfiePageModule", function() { return SelfiePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__selfie__ = __webpack_require__(309);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SelfiePageModule = /** @class */ (function () {
    function SelfiePageModule() {
    }
    SelfiePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__selfie__["a" /* SelfiePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__selfie__["a" /* SelfiePage */]),
            ],
        })
    ], SelfiePageModule);
    return SelfiePageModule;
}());

//# sourceMappingURL=selfie.module.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialPageModule", function() { return TutorialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tutorial__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TutorialPageModule = /** @class */ (function () {
    function TutorialPageModule() {
    }
    TutorialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */]),
            ],
        })
    ], TutorialPageModule);
    return TutorialPageModule;
}());

//# sourceMappingURL=tutorial.module.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicosPageModule", function() { return ServicosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servicos__ = __webpack_require__(310);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ServicosPageModule = /** @class */ (function () {
    function ServicosPageModule() {
    }
    ServicosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__servicos__["a" /* ServicosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__servicos__["a" /* ServicosPage */]),
            ],
        })
    ], ServicosPageModule);
    return ServicosPageModule;
}());

//# sourceMappingURL=servicos.module.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColetasPageModule", function() { return ColetasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__coletas__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ColetasPageModule = /** @class */ (function () {
    function ColetasPageModule() {
    }
    ColetasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__coletas__["a" /* ColetasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__coletas__["a" /* ColetasPage */]),
            ],
        })
    ], ColetasPageModule);
    return ColetasPageModule;
}());

//# sourceMappingURL=coletas.module.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(262);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_informacao_informacao_module__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_selfie_selfie_module__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_cnh_cnh_module__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_login_login_module__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_foto_veiculo_foto_veiculo_module__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_doc_veiculo_doc_veiculo_module__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_tutorial_tutorial_module__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_servicos_servicos_module__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_romaneios_romaneios_module__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_contrato_contrato_module__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_coletas_coletas_module__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_entregas_entregas_module__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_canhoto_canhoto_module__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_geolocation__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_background_geolocation__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_launch_navigator__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_romaneios_listar_romaneios_listar_module__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera_preview__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_firebase_authentication__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_sim__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_selfie_foto_selfie_foto_module__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_home_02_home_02_module__ = __webpack_require__(189);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_foto_veiculo_foto_veiculo_module__["FotoVeiculoPageModule"],
                __WEBPACK_IMPORTED_MODULE_15__pages_doc_veiculo_doc_veiculo_module__["DocVeiculoPageModule"],
                __WEBPACK_IMPORTED_MODULE_11__pages_selfie_selfie_module__["SelfiePageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_cnh_cnh_module__["CnhPageModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_tutorial_tutorial_module__["TutorialPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_servicos_servicos_module__["ServicosPageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_romaneios_romaneios_module__["RomaneiosPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_contrato_contrato_module__["ContratoPageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_coletas_coletas_module__["ColetasPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_entregas_entregas_module__["EntregasPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__pages_canhoto_canhoto_module__["CanhotoPageModule"],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/canhoto/canhoto.module#CanhotoPageModule', name: 'CanhotoPage', segment: 'canhoto', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cnh/cnh.module#CnhPageModule', name: 'CnhPage', segment: 'cnh', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/doc-veiculo/doc-veiculo.module#DocVeiculoPageModule', name: 'DocVeiculoPage', segment: 'doc-veiculo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contrato/contrato.module#ContratoPageModule', name: 'ContratoPage', segment: 'contrato', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/entregas/entregas.module#EntregasPageModule', name: 'EntregasPage', segment: 'entregas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/espaco-veiculo/espaco-veiculo.module#EspacoVeiculoPageModule', name: 'EspacoVeiculoPage', segment: 'espaco-veiculo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/foto-veiculo/foto-veiculo.module#FotoVeiculoPageModule', name: 'FotoVeiculoPage', segment: 'foto-veiculo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home-02/home-02.module#Home_02PageModule', name: 'Home_02Page', segment: 'home-02', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/informacao/informacao.module#InformacaoPageModule', name: 'InformacaoPage', segment: 'informacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/romaneios-listar/romaneios-listar.module#RomaneiosListarPageModule', name: 'RomaneiosListarPage', segment: 'romaneios-listar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/romaneios/romaneios.module#RomaneiosPageModule', name: 'RomaneiosPage', segment: 'romaneios', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selfie-foto/selfie-foto.module#SelfieFotoPageModule', name: 'SelfieFotoPage', segment: 'selfie-foto', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selfie/selfie.module#SelfiePageModule', name: 'SelfiePage', segment: 'selfie', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/servicos/servicos.module#ServicosPageModule', name: 'ServicosPage', segment: 'servicos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/coletas/coletas.module#ColetasPageModule', name: 'ColetasPage', segment: 'coletas', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_17__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    driverOrder: ['localstorage']
                }),
                __WEBPACK_IMPORTED_MODULE_27__pages_romaneios_listar_romaneios_listar_module__["RomaneiosListarPageModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_selfie_foto_selfie_foto_module__["SelfieFotoPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_home_02_home_02_module__["Home_02PageModule"],
                __WEBPACK_IMPORTED_MODULE_0__pages_informacao_informacao_module__["InformacaoPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_10__providers_firebase_firebase__["a" /* FirebaseProvider */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera_preview__["a" /* CameraPreview */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_firebase_authentication__["a" /* FirebaseAuthentication */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_sim__["a" /* Sim */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyDbH22zdcPx9rv0IOMiiGq9lw0T2IhVXKg",
    authDomain: "my-project-1520872899122.firebaseapp.com",
    databaseURL: "https://my-project-1520872899122.firebaseio.com",
    projectId: "my-project-1520872899122",
    storageBucket: "my-project-1520872899122.appspot.com",
    messagingSenderId: "785749493075"
};
//# sourceMappingURL=firebase.config.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfiePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera_preview__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__selfie_foto_selfie_foto__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SelfiePage = /** @class */ (function () {
    function SelfiePage(navCtrl, navParams, camera, firebaseProvider, alertCtrl, loadingCtrl, cameraPreview) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.cameraPreview = cameraPreview;
        // picture options
        this.pictureOpts = {
            width: 1280,
            height: 1280,
            quality: 85,
        };
        this.cameraPreviewOpts = {
            x: 0,
            y: 0,
            width: window.screen.width,
            height: window.screen.height,
            camera: 'rear',
            tapPhoto: true,
            previewDrag: true,
            toBack: true,
            alpha: 1
        };
    }
    SelfiePage.prototype.ngOnInit = function () {
        var _this = this;
        var cameraPreviewOpts = {
            x: 0,
            y: 0,
            width: window.screen.width,
            height: window.screen.height,
            camera: 'rear',
            tapPhoto: true,
            previewDrag: true,
            toBack: true,
            alpha: 1
        };
        // start camera
        this.cameraPreview.startCamera(cameraPreviewOpts).then(function (res) {
            console.log(res);
            _this.cameraPreview.switchCamera();
            _this.iniciarCamera();
        }, function (err) {
            console.log(err);
        });
    };
    SelfiePage.prototype.getUsuario = function (cpf) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__selfie_foto_selfie_foto__["a" /* SelfieFotoPage */], { cpf: cpf });
    };
    SelfiePage.prototype.iniciarCamera = function () {
        this.cameraPreview.show();
    };
    SelfiePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-selfie',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/selfie/selfie.html"*/'<ion-header>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h4>DIGITE seu CPF e clique em PROXIMO!</h4>\n\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/selfie/selfie.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera_preview__["a" /* CameraPreview */]])
    ], SelfiePage);
    return SelfiePage;
}());

//# sourceMappingURL=selfie.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tutorial_tutorial__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ServicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ServicosPage = /** @class */ (function () {
    function ServicosPage(navCtrl, navParams, firebaseProvider, storage, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebaseProvider = firebaseProvider;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.servicos = [];
        this.items = [];
    }
    ServicosPage.prototype.ngOnInit = function () {
        var _this = this;
        this.firebaseProvider.getAllDoc('romaneios').then(function (snapshot) {
            var items = snapshot.val();
            for (var key in items) {
                _this.items.push(items[key]);
            }
            console.log(_this.items);
        });
    };
    ServicosPage.prototype.aceitar = function (item) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Pré Cadastro',
            message: "Para ver detalhes do frete , você precisa se cadastrar",
            buttons: [
                {
                    text: 'Não sou cadastrado',
                    handler: function () {
                        _this.cadastrar(item);
                    }
                },
                {
                    text: 'Já tenho cadastro',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        prompt.present();
    };
    ServicosPage.prototype.cadastrar = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tutorial_tutorial__["a" /* TutorialPage */], { dados: item });
    };
    ServicosPage.prototype.getItems = function (ev, item) {
        console.log(item);
        // Reset items back to all of the items
        this.items;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                if (item === 'coleta') {
                    return (item['coleta']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1);
                }
                else {
                    return (item['entrega']['uf_cliente'].toUpperCase().indexOf(val.toUpperCase()) > -1);
                }
            });
        }
        else {
            // this.initializeItems();
        }
    };
    ServicosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-servicos',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/servicos/servicos.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Serviços</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n        <ion-searchbar     (ionInput)="getItems($event, \'coleta\' )" placeholder="Coleta UF "></ion-searchbar>\n        <ion-searchbar  (ionInput)="getItems($event, \'entrega\' )" placeholder="Entrega UF "></ion-searchbar>\n    </ion-card-content>\n  </ion-card>\n \n  <ion-card *ngFor="let item of items">\n    <ion-card-content>\n\n      <ion-row no-padding>\n        <ion-card-title >\n          <ion-col col-7 text-rigth>\n            <b>Coleta: </b> {{ item.coleta.cidade_cliente }} / {{ item.coleta.uf_cliente }}\n          </ion-col>\n        </ion-card-title>\n      </ion-row>\n      <ion-row no-padding>\n        <ion-card-title >\n          <ion-col col-7 text-rigth>\n            <b>Entrega:</b> {{ item.entrega.cidade_cliente }} / {{ item.entrega.uf_cliente }}\n          </ion-col>\n        </ion-card-title>\n      </ion-row>\n      <hr />\n      <ion-row no-padding>\n        <ion-col col-7 text-rigth>\n          <b class=\'resaltar\'>R$ Frete:</b>{{item.coleta.frete_motorista }}\n        </ion-col>\n      </ion-row>\n      <hr />\n\n\n\n    </ion-card-content>\n    <ion-row no-padding>\n      <ion-col col-12 item-end>\n        <button ion-button round full small color="primary" (click)="aceitar(item)">\n          Aceitar\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/servicos/servicos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ServicosPage);
    return ServicosPage;
}());

//# sourceMappingURL=servicos.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_observable_timer__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_observable_timer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_observable_timer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    //rootPage:any = HomePage;
    function MyApp(platform, statusBar, splashScreen, storage, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        setTimeout(function () {
            _this.initializeApp();
        }, 600);
        storage.get('Login').then(function (val) {
            if (val == 'sim') {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */];
                setTimeout(function () {
                    _this.initializeApp();
                }, 600);
            }
            else {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
                setTimeout(function () {
                    _this.initializeApp();
                }, 600);
            }
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            var funcaoRetorno = function (data) {
                console.log('Notificações: ' + JSON.stringify(data));
            };
            window["plugins"].OneSignal.startInit("cf4a2141-103c-4506-99ac-c4ebf7ec1865", "785749493075")
                .handleNotificationOpened(funcaoRetorno)
                .endInit();
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.show();
            Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_observable_timer__["timer"])(3000).subscribe(function () { return _this.splashScreen.hide(); }); // <-- hide animation after 3s
            //this.pushsetup();
            // statusBar.overlaysWebView(true);
            _this.statusBar.backgroundColorByHexString('#00796B');
            // statusBar.overlaysWebView(true);
            _this.statusBar.backgroundColorByHexString('#00796B');
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__romaneios_romaneios__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_geolocation__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, toastCtrl, alertCtrl, geolocation, navParams, firebaseProvider, backgroundGeolocation, storage) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.navParams = navParams;
        this.firebaseProvider = firebaseProvider;
        this.backgroundGeolocation = backgroundGeolocation;
        this.storage = storage;
        this.entregas = [];
        this.qtdentregas = [];
        this.coletas = {};
        this.servicos = {};
        this.parceiros = {};
        this.data = {};
        this.dadosMotorista = {};
        this.getAllLocalizacao = {};
        this.posicaoCAM = {};
    }
    HomePage.prototype.updateGeolocation = function (dados, lat, lng) {
        var _this = this;
        this.storage.get('mykey').then(function (val) {
            if (val === null) {
                var value = _this.firebaseProvider.createPosicao(dados, lat, lng);
                _this.storage.set('mykey', value);
            }
            else {
                _this.firebaseProvider.atualizarPosicao(val, dados, lat, lng);
            }
        });
    };
    HomePage.prototype.minhaLocalizacao = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            var dados = { nome: _this.dadosMotorista.nome, cpf: _this.dadosMotorista.cpf };
            _this.updateGeolocation(dados, resp.coords.latitude, resp.coords.longitude);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        setTimeout(function () {
            _this.minhaLocalizacaoAutom();
        }, 1000);
    };
    HomePage.prototype.minhaLocalizacaoAutom = function () {
        var _this = this;
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.posicaoCAM.lat = data.coords.latitude;
            _this.posicaoCAM.lng = data.coords.longitude;
            var dados = { nome: _this.dadosMotorista.nome, cpf: _this.dadosMotorista.cpf };
            _this.updateGeolocation(dados, data.coords.latitude, data.coords.longitude);
        });
    };
    HomePage.prototype.abrirRomaneios = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__romaneios_romaneios__["a" /* RomaneiosPage */]);
    };
    HomePage.prototype.abrirParceiros = function () {
        var alert = this.alertCtrl.create({
            title: 'Olá Amigo!',
            subTitle: 'Estamos Desenvolvendo!',
            buttons: ['OK']
        });
        alert.present();
    };
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.firebaseProvider.getAllDoc('romaneios').then(function (snapshot) {
            var items = snapshot.val();
            for (var key in items) {
                if (items[key]['coleta']['status'] === "aberto") {
                    items[key].id = key;
                    _this.qtdentregas.push(items[key]);
                }
            }
        });
        this.minhaLocalizacao();
        this.storage.get('dados').then(function (data) {
            _this.dadosMotorista = data;
        });
        this.startBackgroundGeolocation();
    };
    HomePage.prototype.startBackgroundGeolocation = function () {
        var _this = this;
        this.backgroundGeolocation.isLocationEnabled()
            .then(function (rta) {
            if (rta) {
                _this.start();
            }
            else {
                _this.backgroundGeolocation.showLocationSettings();
            }
        });
    };
    HomePage.prototype.start = function () {
        var _this = this;
        var config = {
            desiredAccuracy: 10,
            stationaryRadius: 1,
            distanceFilter: 1,
            debug: true,
            stopOnTerminate: false,
            // Android only section
            locationProvider: 1,
            startForeground: true,
            interval: 6000,
            fastestInterval: 5000,
            activitiesInterval: 10000
        };
        console.log('start');
        this.backgroundGeolocation
            .configure(config)
            .subscribe(function (location) {
            var watch = _this.geolocation.watchPosition();
            watch.subscribe(function () {
                var dados = { 'nome': _this.dadosMotorista.nome, 'cpf': _this.dadosMotorista.cpf };
                _this.updateGeolocation(dados, location.latitude, location.longitude);
            });
        });
        // start recording location
        this.backgroundGeolocation.start();
    };
    HomePage.prototype.stopBackgroundGeolocation = function () {
        this.backgroundGeolocation.stop();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col width-50>\n        <button ion-button block color="primary">Sua Coleta</button>\n      </ion-col>\n      <ion-col width-50>\n        <button ion-button block color="primary">Sua Entrega</button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col width-50>\n        <button ion-button block color="secondary">Rotas</button>\n      </ion-col>\n      <ion-col width-50>\n        <button ion-button block color="secondary">Seu Cadastro</button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col width-50>\n        <button ion-button block color="danger">Canhotos</button>\n      </ion-col>\n      <ion-col width-50>\n        <button ion-button block color="danger">Espaço no Veiculo</button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col width-50>\n        <button ion-button block color="light">Rede Parceira</button>\n      </ion-col>\n      <ion-col width-50>\n        <button ion-button block color="light">Suporte</button>\n      </ion-col>\n    </ion-row>\n\n      <img class="displayed" src="../../assets/imgs/mapa.png"/>\n          <img class="displayed" src="../../assets/imgs/logo-site.png"/>\n\n\n\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tutorial_tutorial__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_sim__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase_authentication__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, toastCtrl, firebaseProvider, alertCtrl, storage, sim, firebaseAuthentication) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.sim = sim;
        this.firebaseAuthentication = firebaseAuthentication;
        this.fretes = [];
        this.data = {};
        this.statusLogin = false;
        this.show = false;
        this.show2 = false;
        this.code = "";
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        /* const ui = new firebaseui.auth.AuthUI(firebase.auth());
        let config = {
            callbacks: {
                signInSuccessWithAuthResult: function(authResult, redirectUrl) {
                  var user = authResult.user;
                  var credential = authResult.credential;
                  var isNewUser = authResult.additionalUserInfo.isNewUser;
                  var providerId = authResult.additionalUserInfo.providerId;
                  var operationType = authResult.operationType;
                  // Do something with the returned AuthResult.
                  // Return type determines whether we continue the redirect automatically
                  // or whether we leave that to developer to handle.
                  return false;
                },
            },
            signInOptions: [
                firebase.auth.EmailAuthProvider.PROVIDER_ID,
                firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                {
                    provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
                    defaultCountry: 'BR'
                }
            ]
        };

        ui.start('#firebaseui-auth', config); */
        //this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        this.show = false;
        this.status = 0;
        this.data.cpf = '';
        this.data.password = '';
        this.getDadosCelular();
    };
    LoginPage.prototype.authResult = function () {
        (function (authResult) {
            console.log("authResult", authResult);
            return true;
        });
    };
    LoginPage.prototype.acessar = function (cpf) {
        var _this = this;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        this.firebaseProvider.getAllDoc('motoristas').then(function (snapshot) {
            _this.relMotoristas = snapshot.val();
            for (var x in _this.relMotoristas) {
                if (_this.relMotoristas[x].cpf == cpf) {
                    var motorista = _this.relMotoristas[x];
                    if (motorista.device == _this.device) {
                        //(44) 9.9737-0229
                        var celular = motorista.celular.replace(/\D/g, '');
                        _this.signIn(celular, motorista);
                    }
                    else {
                        var prompt_1 = _this.alertCtrl.create({
                            title: 'Erro',
                            message: "Dispositivo não confere com CPF digitado!",
                        });
                        prompt_1.present();
                    }
                }
            }
        });
    };
    LoginPage.prototype.logarPagina = function () {
        this.status = !this.status;
    };
    LoginPage.prototype.cancelar = function () {
        this.navCtrl.setRoot(LoginPage_1);
    };
    LoginPage.prototype.cadastre = function () {
        this.storage.set('device', this.device);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tutorial_tutorial__["a" /* TutorialPage */], { device: this.device });
    };
    LoginPage.prototype.esqueceuSenha = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Login',
            message: "Digite seu CPF para verificação",
            inputs: [
                {
                    name: 'cpf',
                    placeholder: 'CPF'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Enviar',
                    handler: function (data) {
                        _this.verificar(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.verificar = function (value) {
        var _this = this;
        this.firebaseProvider.getAllDoc('motoristas').then(function (snapshot) {
            _this.relMotoristas = snapshot.val();
            for (var x in _this.relMotoristas) {
                if (_this.relMotoristas[x].cpf == value.cpf) {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Sua Senha!',
                        subTitle: _this.relMotoristas[x].cpf,
                        buttons: ['OK']
                    });
                    alert_1.present();
                }
            }
        });
    };
    LoginPage.prototype.mensagemErro = function () {
        var prompt = this.alertCtrl.create({
            title: 'Login',
            message: "Erro ao Autenticar usuario",
        });
        prompt.present();
    };
    /* signIn(phoneNumber: number, motorista) {
        const appVerifier = this.recaptchaVerifier;
        const phoneNumberString = "+55" + phoneNumber;
        firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
            .then(confirmationResult => {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                let prompt = this.alertCtrl.create({
                    title: 'Entre com o codigo',
                    inputs: [{ name: 'confirmationCode', placeholder: 'Confirmação do Codigo' }],
                    buttons: [
                        {
                            text: 'Enviar',
                            handler: data => {
                                if(confirmationResult.verificationId){
                                    this.encaminhar(motorista);
                                }
                            }
                        }
                    ]
                });
                prompt.present();
            })
            .catch(function (error) {
                alert(JSON.stringify(error));
            });

    } */
    LoginPage.prototype.abrirImput = function () {
        this.show = !this.show;
    };
    LoginPage.prototype.signIn = function (phoneNumber, motorista) {
        var phoneNumberString = "+55" + phoneNumber;
        this.firebaseAuthentication.verifyPhoneNumber(phoneNumberString, 60000).then(function (resultado) {
            console.log("RESULTADO DO SIGN IF", resultado);
        });
    };
    LoginPage.prototype.getDadosCelular = function () {
        var _this = this;
        this.sim.hasReadPermission().then(function (info) { return console.log('Has permission: ', info); });
        this.sim.requestReadPermission().then(function () { return _this.permissaoConcedida(); }, function () { return console.log('Permission denied'); });
    };
    LoginPage.prototype.permissaoConcedida = function () {
        var _this = this;
        this.sim.getSimInfo().then(function (info) {
            _this.device = info.deviceId;
        }, function (err) { return console.log('Unable to get sim info: ', err); });
    };
    LoginPage.prototype.encaminhar = function (motorista) {
        this.storage.set('dados', motorista);
        this.storage.set('Login', 'sim');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.validarTelefone = function (e) {
        console.log(e);
        if (e['value'].length == 11) {
            this.show2 = true;
        }
        if (e['value'].length > 11) {
            this.show2 = true;
        }
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/login/login.html"*/'<ion-content class="card-background-page">\n\n    <ion-col col-6>\n        <button ion-button  large  block color="secondary"  (click)="abrirImput()">Digite seu Telefone        </button>\n    </ion-col>\n    <ion-item *ngIf="show">\n        <ion-label floating text-center >DDD + Telefone</ion-label>\n        <ion-input type="text"  [(ngModel)]="data.cpf" name="cpf" (ionChange)="validarTelefone($event)" max-length="11" ></ion-input>\n      </ion-item>\n      <ion-item *ngIf="show2">\n          <button ion-button large block (click)="acessar(data.cpf)">Enviar</button>\n      </ion-item >\n     <ion-col col-6 *ngIf="!show">\n        <button ion-button  large  block (click)="cadastre()">Não tenho cadastro</button>\n   </ion-col>\n      <img class="displayed" src="../../assets/imgs/mapa.png"/>\n      <img class="displayed" src="../../assets/imgs/logo-site.png"/>\n\n\n <!-- <div class="card" >\n		<div class="card-title ">\n			<ion-item>\n				<ion-label stacked>Numero CPF</ion-label>\n				<ion-input type="number" [(ngModel)]="data.cpf" name="cpf" placeholder="Entre com seu Numero de CPF"></ion-input>\n			</ion-item>\n				<ion-col col-6>\n					<button ion-button block large id="sign-in-button" (click)="acessar(data.cpf)">Acessar</button>\n				</ion-col>\n				<ion-col col-6>\n					<button ion-button block large color="danger" (click)="cancelar()">Cancelar</button>\n				</ion-col>\n		</div>\n		<div class="card-subtitle">\n			<button ion-button block large color="ligth" (click)="esqueceuSenha()">Esqueceu a senha!</button>\n		</div>\n	</div> -->\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_sim__["a" /* Sim */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_firebase_authentication__["a" /* FirebaseAuthentication */]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

// 30546980848
// 358240051111110
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocVeiculoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__foto_veiculo_foto_veiculo__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var DocVeiculoPage = /** @class */ (function () {
    function DocVeiculoPage(navCtrl, navParams, camera, firebaseProvider, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    DocVeiculoPage.prototype.takePhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, result, image, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            quality: 50,
                            targetHeight: 600,
                            targetWidth: 600,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            cameraDirection: 0,
                            correctOrientation: true
                        };
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        image = "data:image/jpeg;base64," + result;
                        this.firebaseProvider.uploadImage(this.navParams.get('dados'), image, 'doc-veiculo').then(function (data) {
                            _this.proximaPagina(_this.navParams.get('dados'));
                        }).catch(function (e) { return console.error(e); });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DocVeiculoPage.prototype.proximaPagina = function (value) {
        var loader = this.loadingCtrl.create({
            content: "Por Favor, Aguarde...",
            duration: 3000
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__foto_veiculo_foto_veiculo__["a" /* FotoVeiculoPage */], { dados: value });
        loader.present();
    };
    DocVeiculoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-doc-veiculo',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/doc-veiculo/doc-veiculo.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>DOCUMENTO VEICULO</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 class="dicas" text-center>\n        <h2>Scaneando seus documentos !</h2>\n      </ion-col>\n    </ion-row>\n    <hr />\n    <ion-row class="conteudo">\n      <ion-col col-12 text-center >\n        <h1>Documento Veiculo</h1>\n        <p class="dicas">Faça fotos dos Documentos Veiculos!</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-row class="button">\n    <button ion-button round full (click)="takePhoto()" > Tirar Foto </button>\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/doc-veiculo/doc-veiculo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], DocVeiculoPage);
    return DocVeiculoPage;
}());

//# sourceMappingURL=doc-veiculo.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FotoVeiculoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var FotoVeiculoPage = /** @class */ (function () {
    function FotoVeiculoPage(navCtrl, navParams, camera, firebaseProvider, alertCtrl, loadingCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        storage.get('device').then(function (val) {
            _this.device = val;
        });
    }
    FotoVeiculoPage.prototype.takePhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options, result, image, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            quality: 50,
                            targetHeight: 600,
                            targetWidth: 600,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE,
                            cameraDirection: 0,
                            correctOrientation: true
                        };
                        return [4 /*yield*/, this.camera.getPicture(options)];
                    case 1:
                        result = _a.sent();
                        image = "data:image/jpeg;base64," + result;
                        this.firebaseProvider.uploadImage(this.navParams.get('dados'), image, 'foto-veiculo').then(function (data) {
                            _this.proximaPagina(_this.navParams.get('dados'));
                        }).catch(function (e) { return console.error(e); });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    FotoVeiculoPage.prototype.proximaPagina = function (value) {
        var _this = this;
        this.firebaseProvider.create('cad_Aplicativo', { 'cnpj': value, 'device': this.device });
        var loader = this.loadingCtrl.create({
            content: "Por Favor, Aguarde...",
            duration: 3000
        });
        var alert = this.alertCtrl.create({
            title: 'Seu cadastro será analisado por nossa equipe!',
            subTitle: 'Por favor, aguarde a liberação',
            buttons: [{
                    text: 'OK',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
                    }
                }]
        });
        alert.present();
        loader.present();
    };
    FotoVeiculoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-foto-veiculo',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/foto-veiculo/foto-veiculo.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>FOTOS VEICULO</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 class="dicas" text-center>\n        <h2>Scaneando seus documentos !</h2>\n      </ion-col>\n    </ion-row>\n    <hr />\n    <ion-row class="conteudo">\n      <ion-col col-12 text-center >\n        <h1>Fotos Veiculo</h1>\n        <p class="dicas">Faça fotos dos Veiculos aparecendo a placa!>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-row class="button">\n    <button ion-button round  full (click)="takePhoto()" > Tirar Foto </button>\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/foto-veiculo/foto-veiculo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], FotoVeiculoPage);
    return FotoVeiculoPage;
}());

//# sourceMappingURL=foto-veiculo.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__informacao_informacao__ = __webpack_require__(183);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TutorialPage = /** @class */ (function () {
    function TutorialPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TutorialPage.prototype.proximo = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__informacao_informacao__["a" /* InformacaoPage */], { device: this.navParams.get('device') });
    };
    TutorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tutorial',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/tutorial/tutorial.html"*/'<ion-content class="card-background-page">\n\n  <ion-col col-6>\n    <button ion-button large color="secondary" block >Ainda tenho cadastro! </button>\n  </ion-col>\n  <h6 style="margin-left: 10%; margin-right: 10%"><small>OS PASSOS A SEGUIR FACILITARÃO SEU CADASTRO COMO</small>TRANSPORTADOR UPPERTRUCK</h6>\n  <h6 style="margin-left: 10%; margin-right: 10%"> <b>*</b> <small>PROCURE UM LOCAL COM BOA ILUMINAÇÃO </small> </h6>\n  <h6 style="margin-left: 10%; margin-right: 10%"> <b>*</b> <small>LIMPE AS LENTES DA CÂMERA <br/>&nbsp; &nbsp; &nbsp; &nbsp;  DE SEU SMARTPHONE</small> </h6>\n  <h6 style="margin-left: 10%; margin-right: 10%"> <b>*</b> <small>ENQUADRE AS FOTOS CORRETAMENTE </small></h6>\n  <h6 style="margin-left: 10%; margin-right: 10%"> <b>*</b> <small>UTILIZE O APARELHO NA POSIÇÃO VERTICAL </small></h6>\n\n\n        <div style="margin-top:10%; margin-left: 15%;width:70%; ">\n          <img src="../../assets/imgs/mapa.png" />\n        </div>\n        <div style="margin-top:10%; margin-left:35%;width:30%; ">\n          <img src="../../assets/imgs/logo-site.png" />\n        </div>\n\n        <ion-col col-6>\n          <button ion-button large block color="secondary" (click)="proximo()">Proximo! </button>\n        </ion-col>\n        <!-- <div class="card" >\n  <div class="card-title ">\n    <ion-item>\n      <ion-label stacked>Numero CPF</ion-label>\n      <ion-input type="number" [(ngModel)]="data.cpf" name="cpf" placeholder="Entre com seu Numero de CPF"></ion-input>\n    </ion-item>\n      <ion-col col-6>\n        <button ion-button block large id="sign-in-button" (click)="acessar(data.cpf)">Acessar</button>\n      </ion-col>\n      <ion-col col-6>\n        <button ion-button block large color="danger" (click)="cancelar()">Cancelar</button>\n      </ion-col>\n  </div>\n  <div class="card-subtitle">\n    <button ion-button block large color="ligth" (click)="esqueceuSenha()">Esqueceu a senha!</button>\n  </div>\n</div> -->\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/tutorial/tutorial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], TutorialPage);
    return TutorialPage;
}());

//# sourceMappingURL=tutorial.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Home_02Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__foto_veiculo_foto_veiculo__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__selfie_foto_selfie_foto__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cnh_cnh__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__doc_veiculo_doc_veiculo__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the Home_02Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Home_02Page = /** @class */ (function () {
    function Home_02Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Home_02Page.prototype.ionViewDidLoad = function () {
        var config = this.navParams.get('config');
        this.cpf = config.cpf;
        this.device = config.device;
    };
    Home_02Page.prototype.selfie = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__selfie_foto_selfie_foto__["a" /* SelfieFotoPage */]);
    };
    Home_02Page.prototype.cnh = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cnh_cnh__["a" /* CnhPage */]);
    };
    Home_02Page.prototype.crlv = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__doc_veiculo_doc_veiculo__["a" /* DocVeiculoPage */]);
    };
    Home_02Page.prototype.fotoVeiculo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__foto_veiculo_foto_veiculo__["a" /* FotoVeiculoPage */]);
    };
    Home_02Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home-02',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/home-02/home-02.html"*/'<ion-content padding>\n    <br><br>  <br><br>\n\n\n  <button ion-button large block (click)="selfie()" color="secondary">Faça uma selfie! </button>\n  <br><br>  <br><br>\n\n  <button ion-button large block (click)="cnh()" color="secondary">Tira foto da sua CNH!! </button>\n  <br><br>  <br><br>\n\n\n  <button ion-button large block (click)="crlv()" color="secondary">Tire foto do CRLV! </button>\n  <br><br>  <br><br>\n\n\n  <button ion-button large block (click)="fotoVeiculo()" color="secondary">Tire foto do Veiculo! </button>\n</ion-content>\n'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/home-02/home-02.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */]])
    ], Home_02Page);
    return Home_02Page;
}());

//# sourceMappingURL=home-02.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfieFotoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera_preview__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_02_home_02__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var SelfieFotoPage = /** @class */ (function () {
    function SelfieFotoPage(navCtrl, navParams, camera, firebaseProvider, alertCtrl, loadingCtrl, cameraPreview) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.cameraPreview = cameraPreview;
        // picture options
        this.pictureOpts = {
            width: 1280,
            height: 1280,
            quality: 85,
        };
        this.cameraPreviewOpts = {
            x: 0,
            y: 0,
            width: window.screen.width,
            height: window.screen.height,
            camera: 'rear',
            tapPhoto: true,
            previewDrag: true,
            toBack: true,
            alpha: 1
        };
        this.cpf = this.navParams.get('cpf');
    }
    SelfieFotoPage.prototype.ngOnInit = function () {
        var _this = this;
        this.hide = true;
        var cameraPreviewOpts = {
            x: 0,
            y: 0,
            width: window.screen.width,
            height: window.screen.height,
            camera: 'rear',
            tapPhoto: true,
            previewDrag: true,
            toBack: true,
            alpha: 1
        };
        // start camera
        this.cameraPreview.startCamera(cameraPreviewOpts).then(function (res) {
            console.log(res);
            _this.cameraPreview.switchCamera();
            _this.iniciarCamera();
        }, function (err) {
            console.log(err);
        });
    };
    SelfieFotoPage.prototype.takePhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var result, image;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.hide = false;
                        return [4 /*yield*/, this.cameraPreview.takePicture(this.pictureOpts)];
                    case 1:
                        result = _a.sent();
                        image = "data:image/jpeg;base64," + result;
                        this.firebaseProvider.uploadImage(this.cpf, image, 'selfie').then(function (data) {
                            _this.proximaPagina(_this.cpf);
                        }).catch(function (e) { return console.error(e); });
                        return [2 /*return*/];
                }
            });
        });
    };
    SelfieFotoPage.prototype.proximaPagina = function (value) {
        var loader = this.loadingCtrl.create({
            content: "Por Favor, Aguarde...",
            duration: 3000
        });
        this.cameraPreview.stopCamera();
        this.navCtrl.popTo(__WEBPACK_IMPORTED_MODULE_5__home_02_home_02__["a" /* Home_02Page */]);
        loader.present();
    };
    SelfieFotoPage.prototype.iniciarCamera = function () {
        this.cameraPreview.show();
    };
    SelfieFotoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-selfie',template:/*ion-inline-start:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/selfie-foto/selfie-foto.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n      <ion-title>Cadastro</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding style="background-color: transparent !important;">\n \n  <ion-fab center bottom *ngIf="hide">\n    <button ion-fab color="light" (click)="takePhoto()"><ion-icon name="camera"></ion-icon></button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/marceloandradedasilva/Developer/Documents/Aplicativos/uppertruck/src/pages/selfie-foto/selfie-foto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera_preview__["a" /* CameraPreview */]])
    ], SelfieFotoPage);
    return SelfieFotoPage;
}());

//# sourceMappingURL=selfie-foto.js.map

/***/ })

},[239]);
//# sourceMappingURL=main.js.map